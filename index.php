<?php

set_include_path( get_include_path() 
				. PATH_SEPARATOR . dirname(__FILE__) 
				. PATH_SEPARATOR . dirname(__FILE__) . '/Conf'
				. PATH_SEPARATOR . dirname(__FILE__) . '/Bean' );

require_once 'Core/Controller.php';
require_once 'Clue/Kernel/Engine.php';
require_once 'Clue/Kernel/Loader.php';

set_time_limit( 0 );

spl_autoload_register(array('Clue_Kernel_Loader', 'autoload'));
Clue_Kernel_Engine::init();

//set_error_handler(array('Core_Error', 'handler'), E_WARNING |  E_USER_ERROR | E_USER_WARNING);



Core_Controller::dispatch();

?>
