<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Stacked Tree Viewer</title>
	<link rel="stylesheet" type="text/css" href="styles/theme/ui.all.css" />
	<link rel="stylesheet" type="text/css" href="styles/default.css" />	
	<link rel="stylesheet" type="text/css" href="styles/chart.css" />
	<link rel="stylesheet" type="text/css" href="styles/farbtastic.css" />
	<link rel="icon" type="image/png" href="images/favicon.png"/>
	<script type="text/javascript" src="scripts/jquery.js"></script>
	<script type="text/javascript" src="scripts/jquery-ui.js"></script>
	<script type="text/javascript" src="scripts/jquery.timers.js"></script>
	<script type="text/javascript" src="scripts/farbtastic.js"></script>
	<script type="text/javascript" src="scripts/jquery.selecttouislider.js"></script>
	<script type="text/javascript" src="scripts/jquery.modal.js"></script>
	<script type="text/javascript" src="scripts/canvastext.js"></script>
	<script type="text/javascript" src="scripts/main.js"></script>
	<style type="text/css">
	.Chart .Bar .Graph {
		max-height: <?php echo floatval(Clue_Kernel_Conf::get( 'stv.chart.bar.unit', 3 )); ?>px;
	}
	
	.Chart .SelectedGraph {
		max-height: <?php echo floatval(Clue_Kernel_Conf::get( 'stv.chart.bar.unit', 3 )); ?>px;
		min-height: <?php echo floatval(Clue_Kernel_Conf::get( 'stv.chart.bar.selectedgraph.min', 3 )); ?>px;
	}
	</style>
</head>
<body onunload="MenuActions.saveCollection()">

	<div class="MainContent">
	<?php if( !isset($_SESSION['user']) ) : ?>
	<?php echo $this->action( 'AuthPage' ); ?>
	<?php else : ?>


	<ul class="Menubar">
		<li class="Menucontainer"><h3><?php echo $_SESSION['user']->getLogin(); ?></h3>
			<ul class="Menulist">
				<li action="myPanel()">My panel</li>
				<li action="logout()">Log out</li>
			</ul>
		</li>
		<li class="Menucontainer"><h3>File</h3>
			<ul class="Menulist">
				<li action="displayNewCollectionDialog()">New collection</li>
				<li require_collection="true" class="Disabled" action="saveCollection()">Save</li>
				<li require_collection="true" class="Disabled" action="takeSnapshot()">Save As</li>
				<li require_collection="true" class="Disabled" action="resetCollection()">Reset</li>
			</ul>
		</li>
		<li class="Menucontainer Collections"><h3>Select</h3>
		<ul class="Menulist" ajax="CollectionsListAction">
			<li><img class="Loading" src="images/loading.gif" /></li>
		</ul>
		<li class="Menucontainer Snapshots Disabled" require_collection="true"><h3>Snapshots</h3>
			<ul class="Menulist">
				<li><img class="Loading" src="images/loading.gif" /></li>
			</ul>
		</li>
	</ul>
	
	<div class="Content">

		<?php if( !$_SESSION['user']->hasCurrentCollection() ) : ?>
		<?php echo $this->action( 'MyPanel' ); ?>
		<?php else : ?>
		<?php echo $this->action( 'Collection' ); ?>
		<?php endif ?>
	
	</div>
	
	<div class="Background"></div>
	
	<iframe name="hidden" onload="$('ul[ajax=CollectionsListAction]').trigger( 'refresh' );$('div[ajax]').trigger( 'refresh-div' );" src=""></iframe>
	<?php endif ?>

	</div>

</body>
</html>
