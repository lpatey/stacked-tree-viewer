<?php if( count( $this->getCollectionsList() ) == 0 ) : ?>
<li class="Disabled">There is no collection</li>
<?php else : ?>

	<?php foreach( $this->getCollectionsList() as $collection ) : ?>
		<li key="<?php echo htmlentities($collection->getFolder()); ?>" class="<?php echo $collection->isAvailable() ? '' : 'Disabled'; ?> <?php echo $this->getUser()->hasCurrentCollection() && $collection->getFolder() == $this->getUser()->getCurrentCollection()->getFolder() ? 'Selected' : ''; ?>" action="loadCollection( '<?php echo str_replace( '\'', '\\\'', $collection->getFolder() ); ?>' )">
		<?php echo $collection->getName(); ?></li>
	<?php endforeach ?>
	
<?php endif ?>
