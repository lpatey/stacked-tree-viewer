<div class="Tools">
	<h1><?php echo $this->getMainChart()->getTitle(); ?></h1>
	
	<div class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all RootPosition">
		<div class="Part" style="left: <?php echo $this->getCollection()->getLeftSize()*100/$this->getCollection()->getSize(); ?>%;
			width: <?php echo $this->getCollection()->getPercentageOfWholeCollection(); ?>%"></div>
	</div>
	
	<?php if( count($this->getCollection()->getRootPath() ) > 1 ) : ?>
	
	<select id="RootClusters" name="rootClusters">
		<?php foreach( $this->getCollection()->getRootPath() as $cluster_id => $cluster_name ) : ?>
			<option value="<?php echo $cluster_id; ?>" <?php echo $cluster_id == $this->getCollection()->getRoot() ? 'selected="selected"' : ''; ?>><?php echo $cluster_name; ?></option>
		<?php endforeach; ?>
	</select>
			
	<?php endif; ?>
	
</div>

<?php echo $this->getMainChart(); ?>