
<link rel="stylesheet" type="text/css" href="styles/mypanel.css" />
<script type="text/javascript" type="text/css" src="scripts/form.js"></script>
<div class="Panel">

<div class="MyColumn">

	<h1>My account</h1>

	<fieldset>
		<legend><img src="images/authenticate.png" /> Change password</legend>
		<form method="post" action="UpdatePasswordAction">
		<p class="Message"></p>
		<dl>
				<dt>Old password :</dt>
				<dd><input type="password" name="old_password" /></dd>
				<dt>New password :</dt>
				<dd><input type="password" name="password" /></dd>
				<dt>Confirmation :</dt>
				<dd><input type="password" name="confirmation" /></dd>
				<dd><input type="submit" value="Change" /></dd>
		</dl>
		<div class="Loading">
			<img src="images/ajax-loader.gif" />
		</div>
		</form>
	</fieldset>

	<fieldset>
		<legend><img src="images/internet.png" /> Change email</legend>
		<form method="post" action="UpdateEmailAction">
		<p class="Message"></p>
		<dl>
				<dt>Email :</dt>
				<dd><input type="text" name="email" value="<?php echo htmlentities($this->getUser()->getEmail()) ?>" /></dd>
				<dd><input type="submit" value="Change" /></dd>
		</dl>
		<div class="Loading">
			<img src="images/ajax-loader.gif" />
		</div>
		</form>
	</fieldset>
</div>

<div class="GroupColumn MyColumn">

	<h1>Groups</h1>

	<fieldset>
		<legend><img src="images/create_group.png" /> Create group</legend>
		<form method="post" action="CreateGroupAction">
		<p class="Message"></p>
		<dl>
				<dt>Name :</dt>
				<dd><input type="text" name="name" /></dd>
				<dt>Password <em>(Optional)</em>:</dt>
				<dd><input type="password" name="password" /></dd>
				<dt>Confirmation :</dt>
				<dd><input type="password" name="confirmation" /></dd>
				<dd><input type="submit" value="Create" /></dd>
		</dl>
		<div class="Loading">
			<img src="images/ajax-loader.gif" />
		</div>
		</form>
	</fieldset>

	<fieldset>
		<legend><img src="images/create_group.png" /> Join a group</legend>
		<form method="post" action="JoinGroupAction">
		<p class="Message"></p>
		<dl>
				<dt>Name :</dt>
				<dd><input type="text" name="name" /></dd>
				<dt>Password <em>(Optional)</em>:</dt>
				<dd><input type="password" name="password" /></dd>
				<dd><input type="submit" value="Join" /></dd>
		</dl>
		<div class="Loading">
			<img src="images/ajax-loader.gif" />
		</div>
		</form>
	</fieldset>


</div>

<div class="CollectionColumn MyColumn">

	<h1>Collections</h1>

	<fieldset>
		<legend><img src="images/collections.png" /> My collections</legend>
		<ul ajax="CollectionsListAction" class="MyCollections">
		</ul>
	</fieldset>

	<div ajax="GroupsListAction"></div>

</div>

</div>

<div id="Loading">
	<img src="images/ajax-loader.gif" />
</div>
