<?php if( $this->hasSnapshots() ) : ?>
<?php foreach( $this->getSnapshots() as $snapshot ) : ?>
		<li class="<?php echo $this->getCollection()->getSnapshot() == $snapshot ? 'Selected' : ''; ?>" action="loadSnapshot( '<?php echo str_replace( '\'', '\\\'', $snapshot ); ?>' )"><?php echo $snapshot ?></li>
<?php endforeach ?>
<?php else : ?>
		<li class="Disabled">There is no snapshot</li>
<?php endif ?>