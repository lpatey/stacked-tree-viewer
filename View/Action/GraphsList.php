<?php $criteria = $this->getCluster()->getGraphsCriteria(); ?>
<?php foreach( $this->getCluster()->getOrderedGraphs() as $graph ) : ?>
	<?php $bullet = ''; ?>
	<li
	<?php 
		if( isset( $criteria[$graph->getId()] ) ) {
	 		$fill = '';
	 		$left = '';
	 		$right = '';
	 		$bold = false;
	 		$italic = false;
	 		$leftId = '';
	 		$rightId = '';
	 		$fillId = '';
	 		echo 'style="';
	 		foreach( $criteria[$graph->getId()] as $criterium ) {
	 			switch( $criterium['type'] ) {
	 				case 'fill': 
	 					if( !$fill ) {
	 						echo 'background-color: ' . ($fill = $criterium['color']).";";
	 						$fillId = $criterium['id'];
	 					}
	 					break;
	 				case 'left':
	 					if( !$left ) {
	 						$left = $criterium['color'];
	 						$leftId = $criterium['id'];
	 					}
	 					break;
	 				case 'right' : 
	 					if( !$right ) {
	 						$right = $criterium['color'];
	 						$rightId = $criterium['id'];
	 					}
	 					break;
	 				case 'bold' :
	 						$bold = true;
	 					break;
	 				case 'italic' :
	 						$italic = true;
	 					break;
	 				default :
	 					trigger_error( 'Unknown criterium type', E_USER_ERROR );
	 			}
	 		}
			echo '"';
		} ?> graph_id="<?php echo $graph->getId(); ?>"
		
			<?php if( isset($leftId) && $leftId ) : ?>left_id="<?php echo $leftId; ?>"<?php endif?> 
			<?php if( isset($rightId) && $rightId ) : ?>right_id="<?php echo $rightId; ?>"<?php endif?> 
			<?php if( isset($fillId) && $fillId ) : ?>fill_id="<?php echo $fillId; ?>"<?php endif?>
			>
			<div style="background-color: <?php echo isset($left) ? $left : ''; ?>">&nbsp;</div>
			<div style="background-color: <?php echo isset($right) ? $right : ''; ?>">&nbsp;</div>
			<div class="Name" style="<?php echo isset($bold) ? ($bold ? 'font-weight:bold;' : '') : '' ?><?php echo isset($italic) ? ($italic ? 'font-style:italic;' : '') : '' ?>"><?php echo $graph->getName(); ?></div>
		</li>
<?php endforeach ?>
