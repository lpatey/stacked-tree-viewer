<div class="TakeSnapshotDialog">
<form method="post" action="index.php">
<?php if( $this->hasSnapshots() ) : ?>
	<input type="hidden" name="snapshot" value="" />
<fieldset>
	<legend>Override a snaphot</legend>
	<ul>
<?php foreach( $this->getSnapshots() as $snapshot ) : ?>
		<li><?php echo $snapshot ?></li>
<?php endforeach ?>
	</ul>
</fieldset>
<?php endif ?>
<fieldset>
	<legend><?php echo $this->hasSnapshots() ? 'Or create a new snapshot' : 'New snapshot'; ?></legend>
	<p><input type="text" name="newSnapshot" /> <input type="submit" value="Save" /></p>
</fieldset>

<p><input type="button" name="Cancel" value="Cancel" /></p>

</form>

</div>