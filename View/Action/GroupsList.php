<?php foreach( $this->getGroups() as $group ) : ?>
<fieldset key="<?php echo $group->getKey(); ?>">
	<legend><img src="images/collections.png" /> <?php echo $group->getName(); ?> <input type="button" value="Leave" /></legend>
	<ul ajax="CollectionsListAction?group=<?php echo $group->getKey(); ?>" class="MyCollections">
	</ul>
</fieldset>
<?php endforeach ?>
