	<div class="Left Column">
		<p>
		<span name="sortAsc" class="SortAsc <?php echo $this->getCollection()->isSortAsc() ? 'AscUp' : ''; ?>" value="<?php echo $this->getCollection()->isSortAsc(); ?>">&nbsp;</span>
		
		<select name="sort_criterion">
			<option attribute="value" value="">Sort by typicality</option>
			<?php foreach( $this->getCollection()->getMergedAttributes() as $attribute ) : ?>
				<option <?php echo $this->getCollection()->isValueAttribute( $attribute ) ? 'attribute="value"' : '' ?> <?php echo $this->getCollection()->getSortCriterion() == $attribute ? 'selected="selected"' : '' ?> value="<?php echo $attribute; ?>" >Sort by <?php echo $attribute; ?></option>
			<?php endforeach ?>
		</select>

		<select name="positionBy" <?php echo $this->getCollection()->isValueAttribute() ? '' : 'disabled="disabled"'; ?>>
			<option <?php echo $this->getCollection()->getPositionPolicy() == $this->getCollection()->POSITION_BY_VALUE ? 'selected="selected"' : ''; ?> value="<?php echo $this->getCollection()->POSITION_BY_VALUE?>">Items by value</option>
			<option <?php echo $this->getCollection()->getPositionPolicy() == $this->getCollection()->POSITION_BY_INDEX ? 'selected="selected"' : ''; ?> value="<?php echo $this->getCollection()->POSITION_BY_INDEX?>">Items by index</option>
		</select>
	
		</p>
		
		<div class="Box GraphsList" <?php if( $this->getCollection()->getCurrentCluster() ) : ?>
		cluster_id="<?php echo $this->getCollection()->getCurrentCluster()->getId() ?>"
		<?php endif ?>>
		<h4>Graphs list</h4>
		<ol message="&lt;li class='Disabled'&gt;Select a cluster&lt;/li&gt;">
			<?php if( $this->getCollection()->getCurrentCluster() ) : ?>
				<?php echo $this->action( 'GraphsList' ) ?>
			<?php else : ?>
				<li class="Disabled">Select a cluster</li>
			<?php endif ?>
		</ol>
		</div>
	
		<div class="Box">
		<h4>Criteria</h4>
		<form method="post" action="">
		<ul class="Criteria">
			<li><input type="submit" name="apply" value="Apply criteria" />
			<div class="Options Menucontainer">
				<img src="images/options.png" />
				<ul class="Menulist">
					<li action="addCriterionAndSave()">Add criterion</li>
					<li action="removeEmptyCriteria()">Remove empty criteria</li>
					<li action="toggleDisplayAllGraphs()" <?php echo  $this->getCollection()->isDisplayAllGraphs() ? 'class="Selected"' : ''; ?>>Display all graphs</li>
				</ul>
			</div></li>
			<li class="Template Criterion">
			<div class="Color Options Menucontainer">
				<ul class="Menulist">
					<li><div class="ColorPicker"></div>
					<p><input type="button" name="Cancel" value="Cancel" />
					<input type="button" name="Ok" value="Ok" />
					</p>
					</li>
				</ul>
			</div>
			<select name="criterion">
				<option value="">typicality</option>
			<?php foreach( $this->getCollection()->getMergedAttributes() as $attribute ) : ?>
				<option value="<?php echo $attribute; ?>"><?php echo $attribute; ?></option>
			<?php endforeach ?>
			</select>
			<select name="operator">
				<option value="&lt;">&lt;</option>
				<option value="&gt;">&gt;</option>
				<option value="==">=</option>
				<option value="!=">&ne;</option>
			</select>
			<input type="text" name="value" /> 
			<select name="type">
				<option value="fill">Fill</option>
				<option value="left">Left</option>
				<option value="right">Right</option>
				<option value="bold">Bold</option>
				<option value="italic">Italic</option>
			</select>
			</li>
		</ul>
		</form>
		</div>
		
		<div class="ClustersNumber Box">
			<h4>Clusters</h4>
			<div>
			<img class="Left Cluster" src="images/cluster-left.png" alt="Left" />
			<select id="Clusters">
				<?php for( $i=2; $i<=Clue_Kernel_Conf::get( 'stv.clusters.max', 64 ); $i++ ) : ?>
					<option value="<?php echo $i; ?>" <?php if( $i==$this->getCollection()->getClustersNumber() ) echo 'selected="selected"'; ?>><?php echo $i; ?></option>
				<?php endfor ?>
			</select>
			<img class="Right Cluster" src="images/cluster-right.png" alt="Right" />
			
				<div class="Position">
					<p>Distance 
					<select name="clusterMode">
						<option <?php echo $this->getCollection()->getClusterMode() == $this->getCollection()->HEIGHT_PROPORTIONAL ? 'selected="selected"' : ''; ?> value="<?php echo $this->getCollection()->HEIGHT_PROPORTIONAL?>">proportional</option>
						<option <?php echo $this->getCollection()->getClusterMode() == $this->getCollection()->HEIGHT_NOT_PROPORTIONAL ? 'selected="selected"' : ''; ?> value="<?php echo $this->getCollection()->HEIGHT_NOT_PROPORTIONAL?>">not proportional</option>					
					</select></p>
				</div>
			</div>
		</div>
		
		<div class="Fixed Box">
			<h4>Bloc notes</h4>
			<div class="Comments" style="height: <?php echo Clue_Kernel_Conf::get('stv.notepad.height' ); ?>" contenteditable="true"><?php $content = @file_get_contents( $this->getCollection()->getCommentsPath() ); echo $content ? $content : '<br />'; ?></div>
		</div>
		
	</div>
	
	<div class="Main">

		<?php echo $this->action( 'MainChart' ); ?>
	
	</div>
	
	<div class="Right Column">
	
		<div class="ViewContainer">
		
			<div class="Options Menucontainer">
				<img src="images/options.png" />
				<ul class="Menulist">
					<li action="addView()">Add cluster view</li>
					<li class="RemoveView" action="removeView()">Remove cluster view</li>
				</ul>
			</div>
		
			<div class="Template Box Cluster View">
				<h4>Select a cluster</h4>
				<div><p><img src="images/molecule.png" /></p></div>
			</div>
			
			<div class="Selected Box Cluster View">
				<h4>Select a cluster</h4>
				<div><p><img src="images/molecule.png" /></p></div>
			</div>
		
		</div>
		
		<div class="ViewContainer">
			<div class="Options Menucontainer">
				<img src="images/options.png" />
				<ul class="Menulist">
					<li action="addView()">Add graph view</li>
					<li class="RemoveView" action="removeView()">Remove graph view</li>
				</ul>
			</div>
			
			<div class="Template Box Graph View">
				<h4>Select a graph</h4>
				<p><img src="images/molecule.png" /></p>
				<div class="Box"><h4>Similar graphs</h4><div></div></div>
			</div>
			
			<div class="Selected Box Graph View">
				<h4>Select a graph</h4>
				<p><img src="images/molecule.png" /></p>
				<div class="Box"><h4>Similar graphs</h4><div></div></div>
			</div>
			
			<div class="Box Graph View">
				<h4>Select a graph</h4>
				<p><img src="images/molecule.png" /></p>
				<div class="Box"><h4>Similar graphs</h4><div></div></div>
			</div>
		</div>
	
	</div>

	<script type="text/javascript">
		var alreadyExecuted = false;
		$(function() {

			if( alreadyExecuted ) {
				return;
			}
			alreadyExecuted = true;
			Initializer.initCollection();
		<?php
			foreach( $this->getCollection()->getCriteria() as $criterion ) : ?>
			MenuActions.addCriterion( '<?php echo $criterion['id']; ?>', '<?php echo $criterion['criterion']; ?>', '<?php echo $criterion['operator']; ?>','<?php echo $criterion['value']; ?>', '<?php echo $criterion['type']; ?>', '<?php echo $criterion['color'] ?>');
		<?php 	endforeach;
			for( $i=0; $i<$this->getCollection()->getCriteriaViewNumber() - count($this->getCollection()->getCriteria()); $i++ ) : ?>	
			MenuActions.addCriterion();
		<?php endfor ?>
			Tools.updatePositionPolicy( <?php echo $this->getCollection()->isSortValue() ? 'true' : 'false' ; ?> );
		} );
		</script>