<form method="post" action="UploadCollectionAction" enctype="multipart/form-data" target="hidden" onsubmit="$('.Modal').trigger( 'close' ); return true">
<fieldset>
	<legend>Upload a collection</legend>
	<table>
		<tr>
			<td class="Label">Source :</td>
			<td class="Value"><input type="file" name="source" /></td>
		</tr>
		<tr>
			<td class="Label">Destination :</td>
			<td class="Value" ><select name="destination">
				<option value="">My collections</option>
				<?php foreach( $this->getUser()->getGroups() as $group ) : ?>
					<option value="<?php echo $group->getKey(); ?>"><?php echo $group->getName(); ?></option>
				<?php endforeach ?>
			</select></td>
		</tr>
	</table>
</fieldset>

<p><input type="submit" value="Upload" /> <input type="button" name="Cancel" value="Cancel" /></p>

</form>
