
<link rel="stylesheet" type="text/css" href="styles/authpage.css" />
<script type="text/javascript" type="text/css" src="scripts/form.js"></script>

<div class="Panel">

	<h1>Authentication panel</h1>

	<div class="MyColumn">
		<fieldset>
			<legend><img src="images/authenticate.png" /> Authenticate</legend>

			<form method="post" action="LogInAction">
				<p class="Message"></p>

				<input type="hidden" name="authenticate" value="yes" />
				<dl>
					<dt>Login :</dt>
					<dd><input type="text" name="login" /></dd>
					<dt>Password :</dt>
					<dd><input type="password" name="password" /></dd>
					<dd><input type="submit" value="Log in" /></dd>
				</dl>

				<div class="Loading">
					<img src="images/ajax-loader.gif" />
				</div>

			</form>
		</fieldset>

		<?php if( Clue_Kernel_Conf::get( 'stv.users.allowanonymous', true ) ) : ?>
		<p class="AnonymousAuthenticate">
		<img src="images/anonymous.png" /> 
		Anonymous account</p>
		<?php endif ?>

	</div>

	<?php if( Clue_Kernel_Conf::get( 'stv.users.allowcreation', true ) ) : ?>
	<div class="MyColumn">

	<fieldset>
		<legend><img src="images/create_account.png" /> Create an account</legend>

		<form method="post" action="CreateAccountAction">
			<p class="Message"></p>

			<input type="hidden" name="create_account" value="yes" />
			<dl>
				<dt>Login :</dt>
				<dd><input type="text" name="login" /></dd>
				<dt>Password :</dt>
				<dd><input type="password" name="password" /></dd>
				<dt>Confirmation :</dt>
				<dd><input type="password" name="confirmation" /></dd>
				<dt>Email :</dt>
				<dd><input type="text" name="email" /></dd>
				<dd><input type="submit" value="Create" /></dd>
			</dl>

			<div class="Loading">
				<img src="images/ajax-loader.gif" />
			</div>
		</form>

	</fieldset>

	</div>
	<?php endif ?>

</div>
