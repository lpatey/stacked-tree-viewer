<h4><?php echo $this->getTitle() ?></h4>
<div class="TabContainer" <?php if ($this->isFolded() ) :?> style="display: none" <?php endif?>>
<p>
	<input type="button" name="SelectItem" value="Select" />
	 <select name="BoxOptions">
	<?php foreach( $this->getViews() as $key => $view ) : ?>
	<option value="<?php echo $key ?>" <?php echo $key == $this->getSelectedView() ? 'selected="selected"' : ''; ?>><?php echo $view; ?></option>
	<?php endforeach ?>
</select></p>
<?php foreach( $this->getViews() as $key => $view ) : ?>
<div class="TabPane <?php echo $key; ?>Pane <?php echo ($key == $this->getSelectedView()) ? 'Visible' : ''; ?>">
	<?php $this->getContext()->inc( 'View/BoxView/' . $key . '.php' ); ?>
</div>
	<?php endforeach ?>
</div>
