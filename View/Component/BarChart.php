<div class="Chart <?php echo $this->getCollection()->isSortAsc() ? '' : ' Inverse'; ?>">
	
	<div class="Legend">
	<?php if( $this->hasHierarchyMetric() ) : ?>
	<div class="Hierarchy">
	<?php for( $i=0; $i<=$this->getHierarchyMetric()->getCount(); $i++ ) : ?>
	<div class="LineTitle" style="bottom: <?php echo $this->getHierarchyMetric()->getDistance()*$i + 9.5 + $this->getCollection()->getAllocatedHeight(); ?>%"><?php echo round($this->getHierarchyMetric()->getValue( $i ), 2 ); ?></div>
	<?php endfor ?>
	</div>
	<?php endif ?>
	<div class="Bars">
	<?php for( $i=1; $i<=$this->getBarMetric()->getCount(); $i++ ) : ?>
	<div class="LineTitle" style="bottom: <?php echo ($this->getBarMetric()->getDistance()*$i + 9.5); ?>%"><?php echo $this->getBarMetric()->getValue( $i ); ?></div>
	<?php endfor ?>
	</div>
	</div>


	<div class="BarContainer">
	
	<?php $this->getCollection()->getRootCluster()->displayAsAncestor() ?>
	
	<div class="Legend">
	<?php if( $this->hasHierarchyMetric() ) : ?>
	<div class="Hierarchy">
	<?php for( $i=0; $i<=$this->getHierarchyMetric()->getCount(); $i++ ) : ?>
	<div class="Line" style="bottom: <?php echo $this->getHierarchyMetric()->getDistance()*$i + 10 + $this->getCollection()->getAllocatedHeight(); ?>%"></div>
	<?php endfor ?>
	</div>
	<?php endif ?>
	<div class="Bars">
	<?php for( $i=1; $i<=$this->getBarMetric()->getCount(); $i++ ) : ?>
	<div class="Line" style="bottom: <?php echo ($this->getBarMetric()->getDistance()*$i + 10); ?>%"></div>
	<?php endfor ?>
	</div>
	</div>
	
	
	
	<?php $i=0 ?>
	<?php foreach( $this->getClusters() as $cluster ) : ?>
	<div title="<?php echo '#' . $cluster->getName() . ' ' . round($cluster->getSize()) . ' items'; ?>" 
	class="Bar <?php echo $cluster == $this->getCollection()->getCurrentCluster() ? 'Selected' : '';?>" 
	typical_graph_id="<?php echo $cluster->getTypicalId(); ?>"
	name="<?php echo $cluster->getName(); ?>"
	cluster_id="<?php echo $cluster->getId(); ?>"
	size="<?php echo $cluster->getSize(); ?>"
	style="left: <?php echo $cluster->getLeft(); ?>%; height: <?php echo $cluster->getHeight(); ?>%; width: <?php echo $cluster->getWidth(); ?>%; <?php echo $cluster->getColor() ? 'background-color: ' . $cluster->getColor() : ''?>">
	<?php if( $cluster->getSize() > 1 ) : ?><div class="SetAsRoot"></div><?php endif ?>
	<div class="BarContent" <?php if ($this->isInverseXorByValue() ) : ?>style="top: <?php echo ($this->isInverseXorByValue() ? 1 : -1) *100/$cluster->getSize(); ?>%; bottom: -<?php echo 100/$cluster->getSize(); ?>%"<?php endif ?>>
	<?php $positionPolicy = $this->getPositionPolicy(); ?>
	<?php $size = $cluster->getSize(); ?>
	
	<?php $criteria = $cluster->getGraphsCriteria(); ?>
	<?php foreach( array_reverse($cluster->getOrderedGraphs(),true) as $graph ) : ?>
	<?php $graph_id = $graph->getId(); ?>
	
	<?php if( !isset( $criteria[$graph->getId()] ) && !$this->getCollection()->isDisplayAllGraphs() ) continue;?>
	
	
	<?php 
			$left = '';
	 		$fill = '';
	 		$right = '';
	 		$leftId = '';
	 		$rightId = '';
	 		$fillId = '';
	 		if( isset( $criteria[$graph->getId()] ) ) {
		 		foreach( $criteria[$graph->getId()] as $criterium ) {
		 			switch( $criterium['type'] ) {
		 				case 'fill': 
		 					if( !$fill ) {
		 						$fill = $criterium['color'];
		 						$fillId = $criterium['id'];
		 					}
		 					break;
		 				case 'left':
		 					if( !$left ) {
		 						$left = $criterium['color'];
		 						$leftId = $criterium['id'];
		 					}
		 					break;
		 				case 'right' : 
		 					if( !$right ) {
		 						$right = $criterium['color'];
		 						$rightId = $criterium['id'];
		 					}
		 					break;
		 				case 'bold' :
		 				case 'italic' :
							break;	 				
		 				default :
		 					trigger_error( 'Unknown criterium type', E_USER_ERROR );
		 			}
		 		}
	 		}
	 		
		 ?>
		<div class="Graph"
			 <?php if( $leftId ) : ?>left_id="<?php echo $leftId; ?>"<?php endif?> 
			 <?php if( $rightId ) : ?>right_id="<?php echo $rightId; ?>"<?php endif?> 
			 <?php if( $fillId ) : ?>fill_id="<?php echo $fillId; ?>"<?php endif?> 
			 	 style="background-color: <?php echo $fill ?>; 
				height: <?php echo 100/$cluster->getSize(); ?>%; bottom: <?php $position = ($positionPolicy == $this->getCollection()->POSITION_BY_VALUE ? ($cluster->getNormalizedSortValue( $graph_id )) : $cluster->getSortIndex( $graph_id )/$size ); echo $this->getCollection()->isSortAsc() ? $position*100 : (1-$position)*100; ?>%">
			<?php if ( $left ):?><div class="Left" style="background-color: <?php echo $left; ?>"></div><?php endif ?>
			<?php if ( $right ):?><div class="Right" style="background-color: <?php echo $right; ?>"></div><?php endif ?>
		</div>
	<?php endforeach ?>
		</div>
	</div>
	<div cluster_id="<?php echo $cluster->getId(); ?>" class="BarTitle Item<?php echo ($i%2)+1; ?>" style="left: <?php echo $cluster->getLeft(); ?>%; width: <?php echo $cluster->getWidth(); ?>%; <?php echo $cluster->getColor() ? 'color: ' . $cluster->getColor() : ''?>"><?php echo $cluster->getName(); ?></div>
	<?php $i++; ?>
	<?php endforeach ?>
	
	<div class="BaseLine"></div>
	</div>
	<div class="LineTitle" style="bottom: 9.5%">0</div>
	
	<div id="Loading">
		<img src="images/ajax-loader.gif" />
	</div>
	
</div>
