
<?php if( $this instanceof Action_ClusterBox ) : ?>
<h5><?php echo $this->getGraph()->getName(); ?></h5>
<?php endif ?>

<h5>Similar graphs</h5>
<ol><?php foreach( $this->getGraph()->getSimilarGraphs() as $similarGraph ) : ?>
	<li graph_id="<?php echo $similarGraph['graph']->getId(); ?>" title="<?php echo $similarGraph['sim']*100; ?>%"><?php echo $similarGraph['graph']->getName() ?></li>
	<?php endforeach ?>
</ol>