<h5>Properties</h5>
<dl>
<?php if( $this instanceof Action_ClusterBox ) : ?>
	<dt>Size</dt><dd><?php echo $this->getCluster()->getSize(); ?></dd>
<?php endif ?>

<?php foreach( $this->getGraph()->getProperties() as $name => $property ) : ?>
	<dt><?php echo str_replace( '_', '<span class="Splitter"> </span>_', $name ) ?></dt> <dd><?php echo $property; ?></dd>
<?php endforeach ?>
	<dt>Typicality</dt>
	<dd><?php echo $this->getGraph()->getTypicality(); ?></dd>
</dl>