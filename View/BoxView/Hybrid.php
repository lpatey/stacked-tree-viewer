
<?php if( $this instanceof Action_ClusterBox ) : ?>
<h5><?php echo $this->getGraph()->getName(); ?></h5>
<?php endif ?>

<h5>Graphic representation</h5>
<p><img graph_id="<?php echo $this->getGraph()->getId(); ?>" src="<?php echo $this->getUrl(); ?>" /></p>
<h5>Similar graphs</h5>
<ol><?php foreach( array_slice( $this->getGraph()->getSimilarGraphs(), 0, 5 ) as $similarGraph ) : ?>
	<li graph_id="<?php echo $similarGraph['graph']->getId(); ?>" title="<?php echo $similarGraph['sim']*100; ?>%"><?php echo $similarGraph['graph']->getName() ?></li>
	<?php endforeach ?>
</ol>