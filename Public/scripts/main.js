
var Initializer = {
		
	timeout : {
		clusters : null,
		graphs : null
	},
	
	lastSelectedGraph : null,
	
	state : {
		
		clustersNumber : {
		
			leftHover : false,
			rightHover : false
		}
	},
	
	lastCall : 0,

	init : function() {
	
		Tools.refreshGraphsListHeight();
	
	 	$('#Loading').ajaxStart(function(){
	 		$(this).show();
		});

	 	$('#Loading').ajaxStop(function(){
	 		 $(this).hide();
	 	});

	 	$('.Box', this).each( Initializer.initBox );
	
		$('.Menucontainer',this).each( Initializer.initMenu );

		$(this).each( Initializer.initColumns );
	
		$('.Main',this).each( Initializer.initMain );
	
		$('ul[ajax]',this).each( Initializer.initAjaxMenu );

		$('div[ajax]',this).each( Initializer.initAjaxDiv );

		$('.GroupColumn', this).each( Initializer.initGroupColumn );
	
		$(document).mousedown( Tools.hideAllMenus );
		
		$(window).resize( Tools.refreshGraphsListHeight );
		
		$(window).resize( Tools.synchronizeClient );
		
		Tools.refreshGraphsListHeight();
		
		$(window).init( Initializer.initAccelerators );
	},
	
	initAccelerators : function() {
		
		$(this).keydown( function( e ) {
			
			var now = new Date().getTime();
			if( now - Initializer.lastCall < 100 ) {
				return;
			}
			
			Initializer.lastCall = now;

			if( e.keyCode == $.ui.keyCode.DOWN ) {
				if( Initializer.timeout.graphs ) {
					clearTimeout( Initializer.timeout.graphs );
				}
				if( $('.GraphsList .SelectedGraph').length ) {
					var graph = $('.GraphsList .SelectedGraph').removeClass( 'SelectedGraph').next();
					
				}
				else {
					var graph = $('.GraphsList li:not(.Disabled):first');
				}
				
				Initializer.lastSelectedGraph = graph.attr( 'graph_id' );
				graph.addClass( 'SelectedGraph' ).focus();
				Tools.selectGraphInList();
				Initializer.timeout.graphs = setTimeout( "$('.GraphsList .SelectedGraph').click()", 500 );
				return false;
			}
			
			if( e.keyCode == $.ui.keyCode.LEFT && !Tools.hasSelection() ) {
				if( Initializer.timeout.clusters ) {
					clearTimeout( Initializer.timeout.clusters );
				}
				if( $('.Chart .Selected.Bar').length ) {
					$('.Chart .Selected.Bar').removeClass( 'Selected').prevAll( '.Bar:first' ).addClass( 'Selected' );
				}
				
				if( !$('.Chart .Selected.Bar').length ) {
					$('.Chart .Bar:last').addClass( 'Selected' ).focus();
				}
				Initializer.timeout.clusters = setTimeout( "$('.Chart .Bar.Selected').each( Tools.selectCluster )", 500 );
				return false;
			}

			if( e.keyCode == $.ui.keyCode.RIGHT && !Tools.hasSelection() ) {
				if( Initializer.timeout.clusters ) {
					clearTimeout( Initializer.timeout.clusters );
				}
				if( $('.Chart .Selected.Bar').length ) {
					$('.Chart .Selected.Bar').removeClass( 'Selected').nextAll( '.Bar:first' ).addClass( 'Selected' );
				}
				
				if( !$('.Chart .Selected.Bar').length ) {
					$('.Chart .Bar:first').addClass( 'Selected' ).focus();
				}
				Initializer.timeout.clusters = setTimeout( "$('.Chart .Bar.Selected').each( Tools.selectCluster )", 500 );
				return false;
			}
			
			if( e.keyCode == $.ui.keyCode.UP ) {
				if( Initializer.timeout.graphs ) {
					clearTimeout( Initializer.timeout.graphs );
				}
				if( $('.GraphsList .SelectedGraph').length ) {
					var graph = $('.GraphsList .SelectedGraph').removeClass( 'SelectedGraph').prev();
				}
				else {
					var graph = $('.GraphsList li:last:not(.Disabled)');
				}
				Initializer.lastSelectedGraph = graph.attr( 'graph_id' );
				graph.addClass( 'SelectedGraph' ).focus();
				Tools.selectGraphInList();
				Initializer.timeout.graphs = setTimeout( "$('.GraphsList .SelectedGraph').click()", 500 );
				return false;
			}
			
			if( e.ctrlKey && e.keyCode == 61 ) {
				$('.Main').css( 'width', ($('.Main').width()*1.5)+'px' );
				$('.Main').css( 'height', ($('.Main').height()*1.5)+'px' );
				return false;
			}
			
			if( e.ctrlKey && e.keyCode == 109 ) {
				$('.Main').css( 'width', ($('.Main').width()/1.5)+'px' );
				$('.Main').css( 'height', ($('.Main').height()/1.5)+'px' );
				return false;
			}
		} );
		
	},

	initColumns : function() {

		$('.Left.Column',this).each( Initializer.initLeftColumn );
	
		$('.Right.Column',this).each( Initializer.initRightColumn );
	},
	
	initBox : function() {
		$('> h4', this).click( function() {
			$(this).next().slideToggle( 'fast', Tools.refreshGraphsListHeight );
			$(this).closest( '.Box' ).toggleClass( 'Reduced' );
		} );
		
		$('> .TabContainer > p > select',this).change( function() {
			
			var container = $(this).closest( '.TabContainer' );
			$('> .TabPane:visible',container).removeClass( 'Visible' );
			$('> .' + $(this).attr( 'value' ) + 'Pane', container).addClass( 'Visible' );
		});
	},

	initLeftColumn : function() {
	
		$(this).resizable( {
			ghost : true,
			handles : 'e',
			stop : function() {
				$(this).width( Math.round($(this).width()*100/$(window).width()) + '%' );
				$(this).css( 'height', '100%' );
				$('.Main').css( 'left', $(this).css( 'width' ) );
				$('.Main').width( Math.round(100*($(window).width() - $('.Column:first').width() - $('.Column:last').width()) / $(window).width() )+'%' );
			}
	
		} );
		
		$('.ClustersNumber.Box', this).each( Initializer.initClustersNumberBox );
		
		
		$('select[name=sort_criterion]',this).change( function() {
			$('.GraphsList').attr( 'ajax', 'GraphsListAction?sort_criterion=' + $(this).attr( 'value' ) );
			Tools.updatePositionPolicy( $(this.options[this.selectedIndex]).attr( 'attribute') == 'value' );
			Tools.changeClusters( 'sort_criterion=' + $(this).attr( 'value' ), {}, function() {
				$('.GraphsList').trigger( 'refresh' );
			}  );
		} );
		
		$('.Criteria',this).sortable();
		
		$('.SortAsc', this).click( Tools.changeSortAsc );
		
		$('form:has(.Criteria)',this).submit( Tools.applyCriteria );
	
		$('.GraphsList',this).each( Initializer.initGraphsList );
		
		$('select[name=positionBy]', this).change( function( e ) {
			$(this).each( Tools.setPositionPolicy );
		} );
		
		$('.Comments', this).each( Initializer.initComments );

	},
	
	initClustersNumberBox : function() {
		
		$('#Clusters',this).selectToUISlider( {
			labels : 2,
			sliderOptions : {
				start : function( e, ui ) {
					this.initial = $('#Clusters').attr( 'value' );
				},
				stop : Tools.clustersNumberChanged
			}
		} ).hide();
		
		$('.Right.Cluster', this).hover( Tools.clustersNumber.rightHover, 
				Tools.clustersNumber.rightHout );
		
		$('.Left.Cluster', this).hover( Tools.clustersNumber.leftHover, 
				Tools.clustersNumber.leftHout );
		
		$('.Left.Cluster', this).click( function() {
			var select = $(this).closest( '.Box' ).find( '#Clusters' );
			var value = parseInt($(this).closest( '.Box' ).find( '.ui-slider-tooltip' ).text());
			if( value > 2 ) {
				Tools.setClustersSlider( value - 1 );
			}
		} );
		
		$('.Right.Cluster', this).click( function() {
			var select = $(this).closest( '.Box' ).find( '#Clusters' );
			var value = parseInt($(this).closest( '.Box' ).find( '.ui-slider-tooltip' ).text());
			if( value < select.attr( 'options' ).length + 1 ) {
				Tools.setClustersSlider( value + 1 );
			}
		} );
		
		$('select[name=clusterMode]', this).change( function() {
			Tools.changeClusters( 'cluster_mode=' + $(this).attr( 'value' ) );
		} );
		
	},

	initRightColumn : function() {
		$(this).resizable( {
			ghost : true,
			handles : 'w',
			stop : function() {
			$(this).width( Math.round($(this).width()*100/$(window).width()) + '%' );
			$(this).css( 'height', '100%' );
			$(this).css( 'left', '' );
			$('.Main').width( Math.round(100*($(window).width() - $('.Column:first').width() - $('.Column:last').width()) / $(window).width() )+'%' );
		}
	
		} );
		
		$('.View',this).each( Initializer.initView );
	},
	
	initComments : function() {
		
		$(this).data( 'compare', $('.Comments',this).html() );
		
		//$('.Graph',this).each( Initializer.initGraphItem );
		
		$('*[graph_id]',this).each( Initializer.initClicableItem );
		
		$(this).droppable( {
			hoverClass : 'DroppableOver',
			drop : function( e, ui ) {
				var graph_id = $(ui.draggable).attr( 'graph_id' ); 
				if( typeof  graph_id != 'undefined' ) {
					
					var div = $('<span class="Graph"></span>');
					div.attr( 'graph_id', graph_id);
					div.html( $.trim($(ui.draggable).text()) );
					$(div).each( Initializer.initGraphItem );
					
					var range = window.getSelection().getRangeAt(0);
					if( $(range.startContainer).closest( '.Comments' ).length ) {
						range.insertNode( document.createTextNode( ' ' ) );
						range.insertNode( div.get(0) );
						range.insertNode( document.createTextNode( ' ' ) );
					
					}
					else {
						$(this).append( '&nbsp;' );
						$(this).append( div );
						$(this).append( '&nbsp;' );
					}
				}
			}
		} );
		
		$(this).everyTime( 10*1000, Tools.saveComments );
		
		$(this).blur( Tools.saveComments );
		
	},

	initGraphsList : function() {
	
		$(this).bind( 'refresh', function() {
			if( !$(this).attr( 'cluster_id' ) ) {
				return;
			}
			$('> ol:first', this).html( '<li><img class="Loading" src="images/loading.gif" /></li>' );
			$('> ol:first', this).load( 'GraphsListAction?cluster_id=' + $(this).attr( 'cluster_id' ), function() {
				$('li:not(.Disabled)',this).each( Initializer.initGraphItem );
				Tools.selectGraphInList();
			} );
		} );
		
		$('li:not(.Disabled)',this).each( Initializer.initGraphItem );
	},
	
	initClicableItem : function() {
		
		$(this).click( function() {
			Initializer.lastSelectedGraph = $(this).attr( 'graph_id');
			var o = this;
			$.getJSON( 'GetGraphByIdAction', { 'graph_id' : $(this).attr( 'graph_id') }, function( json ) {
				var div = Tools.selectGraph( json );
				if( !div ) {
					return null;
				}
				var listItem = $(o);
				listItem.addClass( 'SelectedGraph' );
			} );	
		} );
	},
	
	initGraphItem : function() {
		
		if( $(this).hasClass( 'Disabled' ) ) {
			return;
		}
		
		$(this).draggable( {
			helper : Tools.draggingGraphHelper,
			zIndex: 10,
			appendTo : document.body,
			opacity: 0.5,
			scroll : false
		} );
		
		$(this).click( function() {
			Initializer.lastSelectedGraph = $(this).attr( 'graph_id');
			var o = this;
			$.getJSON( 'GetGraphByIdAction', { 'graph_id' : $(this).attr( 'graph_id') }, function( json ) {
				var div = Tools.selectGraph( json );
				if( !div ) {
					return null;
				}
				var listItem = $(o);
				listItem.addClass( 'SelectedGraph' );
			} );	
			
		} );
	},
	
	initCriterion : function() {

		$('.ColorPicker',this).farbtastic( function( color ) {
			var options = $(this.wheel).closest( '.Options' );
			
			if( !options.data( 'previousColor' ) ) {
				options.data( 'previousColor', options.attr( 'color' ) );
			}
			options.css( 'backgroundColor', color );
			options.attr( 'color', color );
			var criterion = $(this.wheel).closest( '.Criterion' );
			var criterion_id = criterion.attr( 'criterion_id' );
			
			$('[fill_id=' + criterion_id + ']').css( 'background-color', color );
			$('[left_id=' + criterion_id + '] > .Left').css( 'background-color', color );
			$('[right_id=' + criterion_id + '] > .Right').css( 'background-color', color );
			
			
		});

		var color = $('.Options',this).attr( 'color' );
		if( color ) {
			$.farbtastic( $('.ColorPicker',this) ).setColor( $('.Options',this).attr( 'color' ) );
		}
		
		$('.Options',this).each( Initializer.initMenu );
		
		$('ul.Menulist',this).bind( 'hide', function() {
			$(this).closest( '.Criterion' ).each( Tools.cancelCriterionColor );
		} );
		
		$('input[name=Cancel]', this).click( function() {
			$(this).closest( '.Criterion' ).each( Tools.cancelCriterionColor );
			Tools.hideAllMenus();
			return false;
		} );
		
		$('input[name=Ok]', this).click( function() {
			$(this).closest( '.Criterion' ).each( Tools.validateCriterionColor );
			Tools.hideAllMenus();
			return false;
		} );
		
	},
	
	initClusterBox : function() {
		
		$(this).each( Initializer.initViewBox );
		
		$('input[name=ClusterName]', this).each( function() {
			
			$(this).data( 'name', $(this).attr( 'value' ) );
		} );
		
		$('input[name=ClusterName]', this).keyup( function() {
			var cluster_id = $(this).closest( '.Box' ).attr( 'cluster_id' );
			var clusters = $('.Cluster.Box[cluster_id=' + cluster_id + ']' );
			clusters.find( '> h4' ).html( $(this).attr( 'value' ) );
			clusters.find( 'input[name=ClusterName]' ).attr(  'value', $(this).attr( 'value' ) );
			$('.Chart .BarTitle[cluster_id=' + cluster_id + ']' ).html( $(this).attr( 'value' ) );
			$('.Chart .BarTitle[cluster_id=' + cluster_id + ']' ).each( Tools.updateBarTitle );
			var bar = $('.Chart .Bar[cluster_id=' + cluster_id + ']' );
			bar.attr( 'name', $(this).attr( 'value' ) );
			bar.attr( 'title', '#' + $(this).attr( 'value' ) + ' ' + bar.attr( 'size' ) + ' items' );
			var o = this;
			$('.Chart .ui-slider-label-show').each( function() {
				var value = $(this).closest( 'li' ).data( 'value' );
				if( value == cluster_id  ) {
					$(this).html( $(o).attr( 'value' ) );
				}
				
			} );
			
		} );
		
		$('form', this).submit( Tools.saveClusterName );
		
		$('.ColorPicker',this).farbtastic( function( color ) {
			var options = $(this.wheel).closest( '.Options' );
			if( !options.data( 'previousColor' ) ) {
				options.data( 'previousColor', options.attr( 'color' ) );
			}
			options.css( 'backgroundColor', color );
			options.attr( 'color', color );
			var cluster = $(this.wheel).closest( '.Box' );
			var cluster_id = cluster.attr( 'cluster_id' );
			
			$('.Chart .Bar[cluster_id=' + cluster_id + ']' ).css( 'background-color', color );
			
			var barTitle = $('.Chart .BarTitle[cluster_id=' + cluster_id + ']' );
			barTitle.css( 'color', color );
			barTitle.each( Tools.updateBarTitle );
			
			
		});
		
		var color = $('.Options',this).attr( 'color' );
		if( color ) {
			$.farbtastic( $('.ColorPicker',this) ).setColor( $('.Options',this).attr( 'color' ) );
		}
		
		$('ul.Menulist',this).bind( 'hide', function() {
			$(this).closest( '.Box' ).each( Tools.cancelClusterColor );
		} );
		
		$('.Options input[name=Cancel]', this).click( function() {
			$(this).closest( '.Box' ).each( Tools.cancelClusterColor );
			Tools.hideAllMenus();
			return false;
		} );
		
		$('.Options  input[name=Delete]', this).click( function() {
			$(this).closest( '.Box' ).each( Tools.deleteClusterColor );
			Tools.hideAllMenus();
			return false;
		} );
		
		$('.Options  input[name=Ok]', this).click( function() {
			$(this).closest( '.Box' ).each( Tools.validateClusterColor );
			Tools.hideAllMenus();
			return false;
		} );
		
		$('input[name=SelectItem]', this).click( function() {
			var box = $(this).closest( '.Box' );
			var cluster_id = box.attr( 'cluster_id' );
			
			if( $('.Chart .Bar[cluster_id=' + cluster_id + ']' ).length ) {
				$('.Chart .Bar[cluster_id=' + cluster_id + ']' ).each( Tools.selectCluster );
			}
			else if( confirm( "This element isn't currently visible\nDo you want to change the root ?" ) ) {
				
				Tools.changeClusters( 'root=' + box.attr( 'root' ) + '&clusters_number=' + box.attr( 'clusters_number' ), {}, function() {
					$('.Chart .Bar[cluster_id=' + cluster_id + ']' ).each( Tools.selectCluster );
				});

			}
		} );
	},
	
	initViewBox : function() {
		
		$(this).each( Initializer.initBox );
		
		$('ol li', this).click( function() {
			
			var o = this;
			$.getJSON( 'GetGraphByIdAction', { 'graph_id' : $(this).attr( 'graph_id') }, function( json ) {
				Tools.unselectGraph();
				var div = Tools.selectGraph( json );
				var listItem = $(o);
				listItem.addClass( 'SelectedGraph' );
			} );	
			
		} );
		
		$('.Options',this).each( Initializer.initMenu );
		
	},
	
	initGraphBox : function() {
		
		$(this).each( Initializer.initViewBox );
		
		$('input[name=SelectItem]', this).click( function() {

			$.getJSON( 'GetGraphByIdAction', { 'graph_id' : $(this).closest( '.Box' ).attr( 'graph_id') }, function( json ) {
				Tools.unselectGraph();
				var div = Tools.selectGraph( json );
				
			} );			
			
		} );
		
	},

	initCollection : function() {
		
		$('*[require_collection=true]').removeClass( 'Disabled' );
		$('.Menucontainer.Snapshots .Menulist').load( 'GetSnapshotsListAction', function() {
			$('> li[action]',this).each( Initializer.initMenuItem );
			Tools.refreshDocumentTitle();
		} );
	},
	
	initMain : function() {
		
		var nClusters = $('.Chart .Bar', this).length;
		Tools.setClustersSlider( nClusters );
		
		$('.Chart .Root .SetRoot', this).mousedown( function( e ) {
			Tools.changeClusters( 'root=' + $(this).attr( 'parent_root' ) );
		} );		
		
		$('.Chart', this ).mousedown( Tools.unselectCluster );
		
		if( Initializer.state.clustersNumber.rightHover ) {
			Tools.clustersNumber.rightHover()
		}
		
		if( Initializer.state.clustersNumber.leftHover ) {
			Tools.clustersNumber.leftHover()
		}
		
		$('.Chart .Bar .BarContent',this).mousedown( function( e ) {

			$('.ui-slider *').blur();
			if( e.ctrlKey ) {
				$(this).each( Tools.changeRoot );
				
				return false;
			}
			

			var position = 1 - (e.pageY - $(this).offset().top)/$(this).height();
			var o = this;
			Initializer.lastSelectedGraph = null;
			$.getJSON( 'GetGraphAtAction', { 'position' : position, 'cluster_id' : $(this).parent().attr( 'cluster_id' ) }, function( json ) {
				Tools.unselectGraph();
				var div = Tools.selectGraph( json );
				Tools.selectGraphInList();
			} );	
			
			if( !$(this).parent().hasClass( 'Selected') ) {
				$(this).parent().each( Tools.selectCluster );
			}
	
			return false;
		} );
	
		$('#RootClusters',this).each( function() {
			$(this).selectToUISlider( {
		
				labels : $('#RootClusters option').length,
				labelSrc : 'text',
				sliderOptions : {
					start : function( e, ui ) {
						
					},
					stop : function( e, ui ) {
						var value = $('#RootClusters').attr( 'value' );
						if( this.initial != value ) {
							Tools.changeClusters( 'root=' + value );
						}
					}
				}
			} ).hide();
		} );
		
		$('.Chart :not(.Root) .SetAsRoot', this).mousedown( function( e ) {
			$(this).parent().each( Tools.changeRoot );
			e.stopPropagation();
		} );
		
		
		$('.Chart .BarTitle', this ).each( Tools.updateBarTitle );
		
		$('.Chart .BarTitle',this).mousedown( function() {
			$('.ui-slider *').blur();
			$('.Chart .Bar[cluster_id=' + $(this).attr( 'cluster_id' ) + ']' ).each( Tools.selectCluster );
			return false;
			
		} );
		
		$('.ui-slider-label-show').hover( function() {
			var value = $(this).closest( 'li' ).data( 'value' );
			$('.ClusterContainer[cluster_id=' + value + '].Selected' ).addClass( 'Hover' );
			
		}, function() {
			var value = $(this).closest( 'li' ).data( 'value' );
			$('.ClusterContainer[cluster_id=' + value + '].Selected' ).removeClass( 'Hover' );
			
		} );
	},

	initView : function() {
		$(this).mousedown( Tools.selectView );
	
	},

	initMenu : function() {
		$(this).mousedown( function( e ) {
			
			if( $(this).hasClass( 'Disabled' ) ) {
				return false;
			}
			
			e.stopImmediatePropagation();
			if( !$('>ul:visible',this).length ) {
				Tools.hideAllMenus();
			}
			$('>ul',this).slideToggle( 'fast' );
			
			Tools.refreshDocumentTitle();
			
			return false;
		} );
	
		$('ul.Menulist',this).mousedown( function() {
			return false;
		} );
	
		$('.Menulist > li[action]',this).each( Initializer.initMenuItem );
	},

	initMenuItem : function() {
		$(this).mousedown( function() {
			if( $(this).hasClass( 'Disabled' ) ) {
				return false;
			}
			Tools.hideAllMenus();
			var action = $(this).attr( 'action' );
			var index = action.indexOf( '(' );
			var method = action.substring( 0, index );
			var params = $.trim(action.substr( index+1, action.length ));
			return eval( 'MenuActions.' + method + '.call' + (params.charAt(0) == ')' ? '(this)' : '(this,' + params) );
	
		} );
	},


	initAjaxMenu : function() {
	
		$(this).bind( 'refresh', function() {
			$(this).html( '<li><img class="Loading" src="images/loading.gif" /></li>' );
			$(this).load( $(this).attr( 'ajax' ), function() {
	
				Initializer.initCollectionList.call( this );
				$('> li[action]',this).each( Initializer.initMenuItem );
			} );
		} );
	
		$(this).trigger( 'refresh' );
	},

	initAjaxDiv : function() {
	
		$(this).bind( 'refresh-div', function() {
			$(this).html( '<img class="Loading" src="images/loading.gif" />' );
			$(this).load( $(this).attr( 'ajax' ), function() {
				$('ul[ajax]', this).each( Initializer.initAjaxMenu );
				$('input', this).each( Initializer.initLeaveButton );
			} );
		} );
	
		$(this).triggerHandler( 'refresh-div' );
	},
	
	initLeaveButton : function() {
		$(this).click( function() {
			var fieldset = $(this).closest( 'fieldset' );
			if( confirm( 'Are you sure you want to leave this group ?' ) ) {
				$.post( 'LeaveGroupAction', { group : fieldset.attr( 'key' ) } );
				fieldset.slideUp( 'fast' );
			}
		} );
	},

	initGroupColumn : function() {
		$('form', this).bind( 'success', function() {
			$('.CollectionColumn div[ajax]').triggerHandler( 'refresh-div' );
		} );
	},

	initSnapshotsDialog : function() {
		
		$('form', this).submit( function() {
			
			$('.Menucontainer.Snapshots .Menulist').load( 'GetSnapshotsListAction',
						{  snapshot : $('input[type=text]',this).attr( 'value' ) }, function() {
				$('> li[action]',this).each( Initializer.initMenuItem );
			} );
			$(this).closest( '.Modal' ).trigger( 'close' );
			return false;
		} );
		
		
		$('li', this).click( function() {
			$('.Menucontainer.Snapshots .Menulist').load( 'GetSnapshotsListAction', 
						{ snapshot : $(this).text() }, function() {
				$('> li[action]',this).each( Initializer.initMenuItem );
			} );
			$(this).closest( '.Modal' ).trigger( 'close' );
		} );
	},

	initCollectionList : function() {
	
		$('> li:not(.Disabled)', this).click( function() {
			if( $(this).hasClass( 'Selected' ) ) {
				return true;
			}
			$( '.Menubar .Selected' ).removeClass( 'Selected' );
	
			$(this).addClass( 'Selected' );
	
		} );
	}
	
}

var MenuActions = {

	displayNewCollectionDialog : function() {
		$(document.body).modal( {
			source : 'UploadCollectionAction',
			validate : function() {}
		} );
		return true;
	},

	logout : function() {
		window.location.href = 'LogOutAction';
	},

	myPanel : function() {
		window.location.href = 'UnselectCollectionAction';
	},

	loadCollection : function( folder ) {
		$('li[key=' + folder + ']').addClass( 'Selected' );
		$('.Content').fadeOut( 'fast', function() {
			$('.Content').load( 'CollectionAction', { folder : folder }, function() {
				Initializer.init.call( this );
				$(this).fadeIn( 'fast' );
			} );
		} );
	
	},
	
	toggleDisplayAllGraphs : function() {
		
		$(this).toggleClass( 'Selected' );
		Tools.changeClusters( 'display_all_graphs=' + ($(this).hasClass( 'Selected' ) ? 1 : 0)  );
		
	},
	
	loadSnapshot : function( snapshot ) {

		$('.Content').fadeOut( 'fast', function() {
			$('.Content').load( 'CollectionAction', { snapshot : snapshot }, function() {
				Initializer.init.call( this );
				$(this).fadeIn( 'fast' );
			} );
		} );
	
	},
	
	resetCollection : function() {
		if( $('.Chart' ).length ) {
			Tools.changeClusters( 'reset=true' );
		}
	},
	
	addView : function() {
		var viewContainer = $(this).closest( '.ViewContainer' );
		var o = viewContainer.find( '.Template.View').clone();
		$(o).hide();
		o.removeClass( 'Template' );
		viewContainer.append( o );
		$(o).each( Initializer.initBox );
		$(o).each( Initializer.initView );
		$(o).slideDown( 'fast', function() {
				$(this).each( Tools.selectView );
				$('.RemoveView', viewContainer).removeClass( 'Disabled' );
		} );
		
	},
	
	removeView : function() {
		var viewContainer = $(this).closest( '.ViewContainer' );
		$('.View.Selected', viewContainer).slideUp( 'fast', function() {
			$(this).remove();
			$('.View:visible:last', viewContainer).each( Tools.selectView );
			if( !$('.View:visible' , viewContainer).length ) {
				$('.RemoveView', viewContainer).addClass( 'Disabled' );
			}
		});
		
	},
	
	addCriterionAndSave : function() {
		MenuActions.addCriterion();
		$.get( 'SetCriteriaViewNumberAction', {
			'criteria_view_number' : $('.Criteria > .Criterion:not(.Template)', $(this).closest( '.Box' ) ).length
		} );
	},
	
	addCriterion : function( id, criterion, operator, value, type, color ) {
		var clone = $('.Criteria .Template').clone();
		clone.removeClass( 'Template' );
		$(clone).hide();
		$(clone).attr( 'criterion_id', new Date().getTime() );
		
		if( arguments.length > 0 ) {
			$(clone).attr( 'criterion_id', id );
			$('select[name=criterion]',clone).attr( 'value', criterion );
			$('select[name=operator]',clone).attr( 'value', operator );
			$('input[name=value]',clone).attr( 'value', value );
			$('select[name=type]',clone).attr( 'value', type );
			$('.Options',clone).css( 'backgroundColor', color );
			$('.Options',clone).attr( 'color', color );
		}
		
		$(clone).each( Initializer.initCriterion );
		$('.Criteria').append( clone );
		$(clone).slideDown( 'fast', Tools.refreshGraphsListHeight );
	},
	
	removeEmptyCriteria : function() {
		$('.Criteria :not(.Template) input:not([value])').each( function() {
			$(this).parent().slideUp( 'fast', function() {
				$(this).remove();
				Tools.refreshGraphsListHeight();
			} );
		} );
		$.get( 'SetCriteriaViewNumberAction', {
			'criteria_view_number' : $('.Criteria :not(.Template) input:[value]', $(this).closest( '.Box' ) ).length
		} );
	},
	
	saveCollection : function() {
		
		$.get( 'SaveCollectionAction' );
	},
	
	takeSnapshot : function() {
		
		$(document.body).modal( {
			source : 'GetSnapshotsDialogAction',
			load : Initializer.initSnapshotsDialog,
			validate : function() {}
		} );
		return true;
	}
}

var Tools = {
		
	hideAllMenus : function() {
		$('ul.Menulist:visible').slideUp( 'fast' ).trigger( 'hide');
	},
	
	refreshGraphsListHeight : function() {
		
		var commentsBlock = $('.Left.Column .Comments').parent();
		var others = (commentsBlock.height() + commentsBlock.offset().top - $('.Criteria' ).closest( '.Box').offset().top);
		var height = $(window).height() - others - 120;
		$('.GraphsList > ol').css( 'height', height + 'px' );
	},
	
	updatePositionPolicy : function( enable ) {
		
		if( enable ) {
			$('select[name=positionBy]').removeAttr( 'disabled' );
		}
		else {
			$('select[name=positionBy]').attr( 'disabled', 'disabled' );
			$('select[name=positionBy]').each( function() {
				this.selectedIndex = 1;
			} );
		}
	},
	
	selectCluster : function() {
		$('.Chart .Bar.Selected' ).removeClass( 'Selected' );
		$(this).addClass( 'Selected' );
		var rootButton = $('.Main .Chart .Root .SetAsRoot');
		$(rootButton).attr( 'parent_root', $(this).attr( 'cluster_id' ) );
		$(rootButton).slideDown( 'fast' );
	
		var o = $('.GraphsList');
		$(o).attr( 'cluster_id', $(this).attr( 'cluster_id' ) );
		$(o).oneTime( 1, function() {
			$(this).trigger( 'refresh' );
		} );
	
		var value = $('.Cluster.View.Selected select[name=BoxOptions]').attr( 'value' );
		if( !value ) {
			value = '';
		}
		
		var clusterView = $('.Cluster.View.Selected');
		var folded = clusterView.hasClass( 'Reduced' ) ? 1 : 0;
		clusterView.attr( 'clusters_number', $('.Chart .Bar').length );
		clusterView.fadeTo( 'fast', 0.5 );
		clusterView.attr( 'root', $('.SetAsRoot:first').parent().attr( 'cluster_id') );
		clusterView.attr( 'cluster_id', $(this).attr( 'cluster_id' ) );
		clusterView.load( 'ClusterBoxAction', { cluster_id : $(this).attr( 'cluster_id' ), view : value, folded : folded }, function() {
			$(this).each( Initializer.initClusterBox );
			$(this).fadeTo( 'fast', 1 );
		});
		
		
	},
	
	
	selectGraph : function(json ) {
	
		if( json['error'] ) {
			if( confirm( "This element isn't currently visible\nDo you want to change the root ?" ) ) {
				var o = this;
				Tools.changeClusters( 'graph_id=' + json[ 'graph_id' ], {}, function() {
					$.getJSON( 'GetGraphByIdAction', { 'graph_id' : json[ 'graph_id' ] }, function( json ) {
						Tools.unselectGraph();
						var div = Tools.selectGraph( json );
						var listItem = $(o);
						listItem.addClass( 'SelectedGraph' );
					} );
				});
				
			}
			return;
		}
		
		if( Initializer.lastSelectedGraph != null
				&& Initializer.lastSelectedGraph != "" + json['graph_id' ] ) {
			return null;
		}
		Tools.unselectGraph();
		var div = $('<div class="SelectedGraph"></div>');
		div.attr( 'graph_id', json['graph_id'] );
		div.css( 'bottom', json['bottom'] + '%' );
		div.css( 'height', (100/parseInt($('.Bar[cluster_id=' + json['cluster_id'] + ']' ).attr( 'size' ))) + '%'  )
		
		$('.Chart .Bar[cluster_id=' + json['cluster_id'] + '] > .BarContent').append( div );
		var o = $('.GraphsList li[graph_id=' + json['graph_id'] + ']' );
		//$('.Selected.Graph.View .Box:visible').slideUp( 'fast' );
		var graphView = $('.Selected.Graph.View.Box:visible');
		var value = $('.Selected.Graph.View select[name=BoxOptions]').attr( 'value' );
		if( !value ) {
			value = '';
		}
		
		var folded = graphView.hasClass( 'Reduced' ) ? 1 : 0;
		graphView.attr( 'graph_id', json['graph_id'] );
		graphView.fadeTo( 'fast', 0.5 );
		graphView.load( 'GraphBoxAction', { graph_id : json['graph_id' ], view : value, folded : folded }, function() {
			$(this).each( Initializer.initGraphBox );
			$(this).fadeTo( 'fast', 1 );
		});
		Tools.selectGraphInList();
		return div;
	},
	
	changeClusters : function( param, form, callback ) {
		$('#ClusterInfos').slideUp( 'fast' );
		
		$('.Main').load( 'MainChartAction?' + param , form, function() {
			Initializer.initMain.call( this );
			$('.GraphsList > ol').html( $('.GraphsList').attr( 'message' ) );
			if( callback ) {
				callback.call( this );
			}
		} );

	},
	
	changeRoot : function() {
		var cluster_id = $(this).attr( 'cluster_id' );
		$(this).effect( 'transfer', { to : '.Main' }, 500, function() {
			Tools.changeClusters( 'root=' + cluster_id );
		} );
	},
	
	draggingGraphHelper : function() {
		var div = $('<div class="DraggingGraph"></div>');
		var graph_id = $(this).attr( 'graph_id');
		div.attr( 'graph_id', graph_id );
		div.html( $.trim( $('.GraphsList [graph_id=' + graph_id + ']').text() ) );
		return div;
	},
	
	setPositionPolicy : function() {
		if( $(this).closest( '.Position' ).find( 'input:disabled' ).length == 0 ) {
			Tools.changeClusters( 'position_policy=' + $(this).attr( 'value' ) );
		}
	},
	
	unselectGraph : function() {
		$('.Chart .SelectedGraph').remove();
		$('.GraphsList .SelectedGraph').removeClass( 'SelectedGraph' );
	},
	
	selectView : function() {
		if( $(this).hasClass( 'Selected' ) ) {
			return;
		}
		$(this).closest( '.ViewContainer' ).find( '.View.Selected' ).removeClass( 'Selected' );

		$(this).addClass( 'Selected' );
	},
	
	saveComments : function() {
		if( $(this).data( 'compare' ) != $(this).html() ) {
			$(this).data( 'compare', $(this).html() );
			$.post( 'SaveCommentsAction', {
				comments : $(this).html()
			} );
		}
	},
	
	applyCriteria : function() {
		
		var form = {};
		form['change_criteria'] = true;
		$('.Criteria > .Criterion').each( function( i ) {
			if( !$('input[name=value]',this).attr( 'value' ) ) {
				return;
			}
			form['criteria['+i+'][id]'] = $(this).attr( 'criterion_id' );
			form['criteria['+i+'][criterion]'] = $('select[name=criterion]',this).attr( 'value' );
			form['criteria['+i+'][operator]'] = $('select[name=operator]',this).attr( 'value' );
			form['criteria['+i+'][value]'] = $('input[name=value]',this).attr( 'value' );
			form['criteria['+i+'][type]'] = $('select[name=type]',this).attr( 'value' );
			form['criteria['+i+'][color]'] = $('.Options',this).attr( 'color' );
			form['criteria['+i+'][criterion_id]'] = i;
		} );
		Tools.changeClusters( '', form, function() {
			$('.GraphsList').triggerHandler( 'refresh' );
		} );
		
		return false;
			
		
	},
	
	changeSortAsc : function() {
		$(this).toggleClass( 'AscUp' );
		$('.Chart').toggleClass( 'Inverse' );
		Tools.changeClusters( 'sort_asc=' + ($(this).hasClass( 'AscUp' ) ? 1 : 0), {}, function() {
			$('.GraphsList').triggerHandler( 'refresh' );
		} );
	},
	
	clustersNumberChanged : function() {
		
		var value = $('#Clusters').attr( 'value' );
		if( this.initial != value ) {
			Tools.changeClusters( 'clusters_number=' + value );
		}
	},
	
	selectGraphInList : function() {
		
		var graph_id = $('.GraphsList .SelectedGraph, .Chart .SelectedGraph').attr( 'graph_id' );
		if( !graph_id ) {
			return;
		}
		
		var listItem = $('.GraphsList li[graph_id=' + graph_id + ']');

		if( !listItem.length ) {
			return;
		}
		
		listItem.addClass( 'SelectedGraph' );
		$('.GraphsList ol').each( function() {
			var currentPosition = this.scrollTop;
			var selectedPosition = $(listItem).offset().top - $(this).offset().top + this.scrollTop;
			var height = $(this).height();
			var itemHeight = $(listItem).outerHeight();
			var diff = selectedPosition  + itemHeight - currentPosition;
			if( diff > height || diff < itemHeight ) {
				this.scrollTop = selectedPosition;
			}
			else {
				this.scrollTop = currentPosition;
			}
		} );
	},
	
	validateCriterionColor : function() {
		$('.Options',this).data( 'previousColor',  $('.Options',this).attr( 'color' ) );
		setTimeout( 'Tools.applyCriteria();', 10 );
	},
	
	cancelCriterionColor : function() {
		$.farbtastic($('.ColorPicker',this)).setColor( $('.Options',this).data( 'previousColor' ) );
	},
	
	setClustersSlider : function( value ) {
		var select = $( '#Clusters' );
		select.attr( 'selectedIndex', value-2 );
		$(select).closest( '.Box' ).find( '.ui-slider-tooltip' ).html( value );
		select.click();
		
		if( $('.Chart .Bar').length != value ) { 
			$(select).closest( '.Box' ).find( '.ui-slider' ).each( Tools.clustersNumberChanged );
		}
		
	},
	
	updateBarTitle : function() {
		
		var bar = $('.Chart .Bar[cluster_id=' + $(this).attr( 'cluster_id' ) + ']');
		var text = bar.attr( 'name' );
		$(this).html( '<canvas></canvas>' );
		var context = this.firstChild.getContext( '2d' );
		CanvasTextFunctions.enable(context);
		context.rotate(30 * Math.PI / 180);
		var color = $(this).css( 'color' );
		context.strokeStyle = color;
		
		context.drawText( 'Arial', 12, 5, 5, text );
		
	},
	
	saveClusterName : function() {
		var value =  $('input', this).attr( 'value' );
		
		if( value == $('input', this).data( 'name' ) ) {
			return false;
		}
		
		$.post( 'SaveClusterNameAction', { 
				cluster_id : $(this).closest( '.Box' ).attr( 'cluster_id' ),
				name : value
		} );
			
		$('input', this).data( 'name', value );
		
		return false;
		
	},
	
	unselectCluster : function() {
		
		if( $('.Chart .Bar.Selected' ).length ) {
			
			$.get( 'UnselectClusterAction' );
			$('.Chart .Bar.Selected' ).removeClass( 'Selected' );
				
		}
	},
	
	deleteClusterColor : function() {
		
		var cluster_id = $(this).attr( 'cluster_id' );
		$('.Color.Options', this).data( 'previousColor', '' );
		$('.Color.Options', this).css( 'background-color', '' );
		$('.Chart .Bar[cluster_id=' + cluster_id + ']' ).css( 'background-color', '' );
		var barTitle = $('.Chart .BarTitle[cluster_id=' + cluster_id + ']' );
		barTitle.css( 'color', '' );
		barTitle.each( Tools.updateBarTitle );
		$.post( 'SaveClusterColorAction', { cluster_id : cluster_id } );
	},
	
	validateClusterColor : function() {
		var cluster_id = $(this).attr( 'cluster_id' );
		var color = $('.Color.Options', this).attr( 'color' );
		$('.Color.Options',this).data( 'previousColor', color );
		$.post( 'SaveClusterColorAction', { cluster_id : cluster_id, color : color } );
	},
	
	cancelClusterColor : function() {
		var color = $('.Options',this).data( 'previousColor' );
		if( color ) {
			$.farbtastic($('.ColorPicker',this)).setColor( color );
		}
	},
	
	synchronizeClient : function() {
		$.post( 'SetClientHeightAction', { 'height' : window.innerHeight } );
	},
	
	hasSelection : function() {
		var anchor = window.getSelection().anchorNode;
		return $(anchor).attr( 'tagName' ) == 'INPUT' || $(anchor).closest( '.Comments' ).length;
	},
	
	clustersNumber : {
		
		rightHover : function() {
		
			Initializer.state.clustersNumber.rightHover = true;
			var max = 0;
			$('.Chart .Bar').each( function() {
				max = Math.max( max, parseInt($(this).attr( 'cluster_id' ) ) );
			} );
			
			var bar = $('.Chart .Bar[cluster_id=' + max + ']');
			$('.Chart .Bar[cluster_id=' + max + ']').addClass( 'Splitable' );
			$('.Chart .ParentCluster[right_cluster_id=' + max + ']').addClass( 'Splitable' );
			$('.Chart .ParentCluster[left_cluster_id=' + max + ']').addClass( 'Splitable' );
			
			
		},
		
		rightHout : function() {
			
			Initializer.state.clustersNumber.rightHover = false;
			var max = 0;
			$('.Chart .Bar').each( function() {
				max = Math.max( max, parseInt($(this).attr( 'cluster_id' ) ) );
			} );
			$('.Chart .Bar[cluster_id=' + max + ']').removeClass( 'Splitable' );
			$('.Chart .ParentCluster[right_cluster_id=' + max + ']').removeClass( 'Splitable' );
			$('.Chart .ParentCluster[left_cluster_id=' + max + ']').removeClass( 'Splitable' );
		},
		
		leftHover : function() {
			
			Initializer.state.clustersNumber.leftHover = true;
			var min = 100;
			var chart = null;
			$('.Chart .ClusterContainer').each( function() {
				var value = parseFloat( $(this).attr( 'head_top' ) );
				if( value < min ) {
					min = value;
					chart = this;
				}
			} );
			$('> .ParentCluster', chart).addClass( 'Mergeable' );
			
		},
		
		leftHout : function() {
			
			Initializer.state.clustersNumber.leftHover = false;
			var min = 100;
			var chart = null;
			$('.Chart .ClusterContainer').each( function() {
				var value = parseFloat( $(this).attr( 'head_top' ) );
				if( value < min ) {
					min = value;
					chart = this;
				}
			} );
			$('> .ParentCluster', chart).removeClass( 'Mergeable' );
		}
		
	},
	
	refreshDocumentTitle : function() {
		
		var title = "Stacked Tree Viewer";
		if( $('.Menucontainer.Collections .Selected' ).length ) {
			title += ' - ' + $.trim($('.Menucontainer.Collections .Selected' ).text());
		}
		if( $('.Menucontainer.Snapshots .Selected' ).length ) {
			title += ' - ' + $.trim($('.Menucontainer.Snapshots .Selected' ).text());
		}
		
		document.title = title;
	}

}

$(document).ready( Initializer.init );
