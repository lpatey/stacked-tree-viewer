

(function($) {

	/**
	Params :
		source : url � appeler
		validate : fonction de callback
	*/
	$.fn.modal = function( options ) {
	
		var div = $('<div></div>' );
		$(div).addClass( 'Modal' );
		$(this).append( div );
		
		$(div).bind( 'validate', options.validate );
		$(div).bind( 'close', close );
		
		$('.Background').fadeIn( 'fast' );
		$(div).load( options.source, function() {
			
			$('input[name=Cancel]',this).click( function() {
				$(this).closest('.Modal').trigger( 'close' );
			} );
			
			if( options.load ) {
				options.load.call( this );
			}
			
			$(div).slideDown( 'fast' );
		} );
	}

	
	function close() {
		$(this).slideUp( 'fast', function() {
			$(this).remove();
			$('.Background').fadeOut( 'fast' );
		} );
	}
	
	
})(jQuery);

$(document).ready( function() {
	$(document).keydown( function( e ) {
		if( e.keyCode == 27 ) {
			$('.Modal').trigger( 'close' );
		}
	} );
} );
