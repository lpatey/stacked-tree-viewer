
$(function() {
	$('form').each( Form.init );
});

var Form = {

	init : function() {

		$(this).submit( function() {
			$('.Message', this).hide();
			$('.Loading', this).show();
			$(this).css( 'opacity', 0.5 );
			var form = this;

			$.post( $(this).attr( 'action' ), Form.getData( this ), function( json ) {
				$('.Loading', form).hide();
				$(form).css( 'opacity', 1 );
				if( json['error'] != undefined ) {
					$(form).closest( 'fieldset' ).effect("shake", { times:2 }, 100, function() {
						$('.Message', form).html( json.message ).slideDown( 'fast' );
					} );
				}
				else if( json['message'] != undefined ) {
					$('.Message', form).html( json.message ).slideDown( 'fast' );
				}
				else if( json['redirect'] != undefined ) {
					window.location.href = json.redirect;
				}

				if( json['success'] != undefined ) {
					$('input[type=password]', form).attr( 'value', '' );
					setTimeout( function() {
						$('.Message', form).slideUp( 'fast' );
					}, 2000 );

					$(form).triggerHandler( 'success', json );
				}
			}, 'json' );

			return false;
		} );
	},

	getData : function( form ) {
		var data = {};
		$('input', form).each( function() {
			data[ $(this).attr( 'name' ) ] = $(this).attr( 'value' );
		} );
		return data;
	}

}
