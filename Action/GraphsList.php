<?php

class Action_GraphsList extends Core_CollectionAction {

	private $_cluster;

	public function execute() {
		
		$collection = $this->getCollection();

		if( isset($_GET['cluster_id'] ) ) {
			$collection->setCurrentCluster( $collection->getCluster( $_GET['cluster_id'] ) );
		}
		
		if( isset($_GET['sort_criterion'] ) ) {
			$collection->setSortCriterion( $_GET['sort_criterion'] );
		}
		
		if( !($this->_cluster = $collection->getCurrentCluster()) ) {
			exit(0);
		}
	
	}
	
	public function getCluster() {
		return $this->_cluster;
	}

}

?>
