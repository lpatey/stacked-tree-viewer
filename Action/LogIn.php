<?php

class Action_LogIn extends Core_Action {

	public function execute() {

		if( !isset($_POST['authenticate']) ) {
			header( 'Location: index.php' );
			exit(0);
		}

		$login = strip_tags( $_POST['login'] );
		$password = $_POST['password'];
		
		$user = new User( $login );
		if( !$user->authenticate( $password ) ) {
			$this->error( 'Invalid credentials' );
		}

		$_SESSION['user'] = $user;
		echo json_encode( array(
			'redirect' => 'index.php'
		) );
		exit();

	}
}
