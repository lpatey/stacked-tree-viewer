<?php

class Action_UpdateEmail extends Core_UserAction {

	public function execute() {

		$email = trim(strip_tags( $_POST['email'] ));
		if( !$email ) {
			$this->error( 'Email empty' );
		}

		$this->getUser()->setEmail($email);
		$this->getUser()->save();

		$this->success( 'Email changed' );
	}
}
