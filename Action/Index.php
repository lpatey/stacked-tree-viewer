<?php

class Action_Index extends Core_Action {

	public function execute() {
	
		if( !isset( $_SESSION['client'] ) ) {
			$this->setView( 'Action/GetClient.php' );
		}
		
		if( isset( $_GET['client' ] ) ) {
			$_SESSION['client'] = new Client( $_GET['height' ] );
			header( 'Location: ' . $_SERVER['PHP_SELF'] );
			exit();
		}
	
	}

}

?>
