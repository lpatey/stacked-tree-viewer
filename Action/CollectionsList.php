<?php

class Action_CollectionsList extends Core_UserAction {

	private $_collectionsList = array();

	public function execute() {

		if( isset($_GET['group']) ) {
			$group = new Group( $_GET['group'] );
			if( !$group->isAccessible() ) {
				$this->error( 'Cannot access to this group' );
			}
			$this->_collectionsList = $group->getCollections();
			return;
		}

		$this->_collectionsList = $this->getUser()->getCollections();
	}
	
	public function getCollectionsList() {
		return $this->_collectionsList;
	}

}

?>
