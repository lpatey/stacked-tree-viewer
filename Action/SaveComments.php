<?php

class Action_SaveComments extends Core_CollectionAction {

	public function execute() {

		$collection = $this->getCollection();
		$comments = $_POST['comments'];
		if( get_magic_quotes_gpc() ) {
			$comments = stripslashes( $comments );
		}
		file_put_contents( $collection->getCommentsPath(), $comments );
		
		exit();
	}
	
	
}

?>
