<?php

class Action_CreateGroup extends Core_UserAction {

	public function execute() {
		
		$name = strip_tags( $_POST['name'] );

		if( !$name ) {
			$this->error( 'Name required' );
		}

		$key = Group::toKey( $name );
		$group = new Group( $key );

		if( $group->exists() ) {
			$this->error( 'Similar name already used' );
		}

		$password = $_POST['password'];
		$confirmation = $_POST['confirmation'];

		if( $password != $confirmation ) {
			$this->error( 'Password and confirmation does not match' );
		}

		$group->create( $name, $this->getUser()->getLogin(), $password );

		$this->getUser()->joinGroup( $group )->save();

		$this->success( 'Group created' );
	}

}

?>
