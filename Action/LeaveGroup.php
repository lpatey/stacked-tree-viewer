<?php

class Action_LeaveGroup extends Core_UserAction {

	public function execute() {
		
		$key = $_POST['group'];

		if( !$key ) {
			$this->error( 'Invalid group' );
		}

		$group = new Group( $key );
		$this->getUser()->leaveGroup( $group )->save();

		$this->success( 'Group left' );
	}
}
