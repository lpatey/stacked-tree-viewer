<?php

abstract class Action_Box extends Core_CollectionAction implements Core_Includer {
	
	protected $_graph;
	protected $_box;
	
	
	public function getBox() {
		return $this->_box;
	}
	
	public function getGraph() {
		return $this->_graph;
	}
	
	public function inc( $path ) {
		include $path;
	}
	
	public function getUrl() {
		
		$graph = $this->getGraph();

		$executable = Clue_Kernel_Conf::get( 'stv.viewer.exec' );
		$win = stripos(PHP_OS, 'win') === 0;
		$file = 'Java/molconvert.' . ($win ? 'bat' : 'sh' ); 
			
		if( $win ) {
			file_put_contents( $file, '@echo off' . "\r\n" . $executable . ' %*' );
		}
		else {
			$executableContent = file_get_contents( $executable );
			$executableContent = preg_replace( '/(javaopts\s*=\s*"[^"]*)"/si', '$1 -Djava.awt.headless=true"', $executableContent );
			$executableContent = preg_replace( '/(mypath=)\$0/si', '$1"' . $executable . '"', $executableContent );
			file_put_contents( $file, $executableContent );
		}
		
		chmod( $file, 0777 );
		
		$name = tempnam( 'stv', 'png' );
		$name2 = tempnam( 'stv', 'png' );
		file_put_contents( $name, $graph->getSerialization() );
		$executable = sprintf( $file . " png %s{sdf:} -o %s", $name, $name2 );
		
		
		echo `$executable 2>&1`;
	
		return 'data:image/png;base64,' . base64_encode( file_get_contents( $name2 ) );
		
	}

}

?>
