<?php

class Action_ClusterBox extends Action_Box {

	private $_cluster;
	
	public function execute() {
	
	
		$cluster_id = $_POST['cluster_id'];
		$this->_cluster = $_SESSION['user']->getCurrentCollection()->getCluster( $cluster_id );
		$this->_graph = $this->_cluster->getTypicalGraph();
		
		$box = new Core_View_Box($this, $this->_cluster->getName());
		$box->add( 'Cluster', 'Cluster view' );
		$box->add( 'Hybrid', 'Hybrid view' );
		$box->add( 'Text', 'Text view' );
		$box->add( 'SimilarGraphs', 'Similar Graphs' );
		
		if( $_POST['view'] ) {
			$box->setSelectedView( $_POST['view'] );
		}
		
		if( $_POST['folded'] ) {
			$box->setFolded( $_POST['folded'] );
		}
		
		$this->_box = $box;

	}
	
	public function getCluster() {
		return $this->_cluster;
	}
	
}

?>
