<?php

class Action_CreateAccount extends Core_Action {

	public function execute() {
		
		if( !$_POST['create_account'] ) {
			header( 'Location: index.php' );
			exit(0);
		}
		
		$login = strip_tags( $_POST['login'] );

		if( !$login ) {
			$this->error( 'Login required' );
		}

		$password = $_POST['password'];
		$confirmation = $_POST['confirmation'];
		if( !$password ) {
			$this->error( 'Password required' );
		}

		if( $password != $confirmation ) {
			$this->error( 'Password and confirmation does not match' );
		}

		$user = new User( $login );

		if( $user->exists() ) {
			$this->error( 'Login already exists' );
		}

		$user->create( $password, $_POST['email'] );

		$_SESSION['user'] = $user;

		echo json_encode( array(
			'redirect' => 'index.php'
		) );
		exit();
	}

}

?>
