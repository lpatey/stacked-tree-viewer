<?php

class Action_GraphBox extends Action_Box {

	public function execute() {
	
	
		$graph_id = $_POST['graph_id'];
		$this->_graph = $this->getCollection()->getGraph( $graph_id );
		
		$box = new Core_View_Box($this, $this->_graph->getName());
		$box->add( 'Picture', 'Picture view' );
		$box->add( 'Hybrid', 'Hybrid view' );
		$box->add( 'Text', 'Text view' );
		$box->add( 'SimilarGraphs', 'Similar Graphs' );
		
		if( $_POST['view'] ) {
			$box->setSelectedView( $_POST['view'] );
		}
		
		if( $_POST['folded'] ) {
			$box->setFolded( $_POST['folded'] );
		}

		$this->_box = $box;

	}
	
}

?>
