<?php

class Action_JoinGroup extends Core_UserAction {

	public function execute() {
	
		$name = $_POST['name'];
		$key = Group::toKey( $name );
		
		if( !$key ) {
			$this->error( 'Unknown group' );
		}

		$group = new Group( $key );
		if( !$group->exists() ) {
			$this->error( 'Unknown group' );
		}

		if( !$group->authenticate( $_POST['password'] ) ) {
			$this->error( 'Invalid password' );
		}

		$groups = $this->getUser()->getGroups();

		if( isset( $groups[$key] ) ) {
			$this->success( 'Group already joined' );
		}

		$this->getUser()->joinGroup( $group )->save();

		$this->success( 'Group joined' );
	}
}
