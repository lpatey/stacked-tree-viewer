<?php

class Action_MainChart extends Core_CollectionAction {

	private $_mainChart = null;
	private $_collection = null;

	public function execute() {
		
		$collection = $this->getCollection();

		if( isset( $_GET['clusters_number'] ) ) {
			$collection->setClustersNumber( $_GET['clusters_number'] );
		}
		
		if( isset( $_GET['graph_id' ] ) ) {
			$collection->setGraphVisible( $_GET['graph_id'] );
		}
		
		if( isset( $_GET['root'] ) ) {
			$collection->setRoot( $_GET['root'] );
		}
		
		if( isset( $_GET['reset'] ) ) {
			$collection->reset();
		}
		
		if( isset( $_GET['display_all_graphs'] ) ) {
			$collection->setDisplayAllGraphs( $_GET['display_all_graphs'] );
		}
		
		if( isset($_GET['sort_criterion'] ) ) {
			$collection->setSortCriterion( $_GET['sort_criterion'] );
		}
		
		if( isset($_GET['sort_asc'] ) ) {
			$collection->setSortAsc( $_GET['sort_asc'] );
		}
		
		if( isset($_GET['cluster_mode'] ) ) {
			$collection->setClusterMode( $_GET['cluster_mode'] );
		}

		$this->_mainChart = new Core_Graph_BarChart( $collection );
	
	}
	
	public function getMainChart() {
		return $this->_mainChart;
	}

}

?>
