<?php

class Action_GetGraphById extends Core_CollectionAction {

	public function execute() {

		$collection = $this->getCollection();
		$graph = $collection->getGraph( $_GET['graph_id'] );
		$found = false;
		foreach( $collection->getClusters() as $cluster ) {
			if( $cluster->contains( $graph ) ) {
				$found = true;
				break;
			}
		}
		if( !$found )  {
			die( json_encode( array( 'error' => 'cluster not found',  'graph_id' => $_GET['graph_id'] ) ) );	
		}
		
		$graphs = $cluster->getGraphsByTypicality();
		$count = count($graphs);
		
		
		

		if( $collection->getPositionPolicy() == $collection->POSITION_BY_INDEX ) {
			
			$bottom = $cluster->getSortIndex( $graph->getId() )/$count;
			
			if( !$collection->isSortAsc() ) {
				$bottom = 1-$bottom;
			}
			
			die( json_encode( array(
				'position' => $cluster->getSortIndex( $graph->getId() ),
				'bottom' =>  $bottom * 100,
				'cluster_id' => $cluster->getId(),
				'name' => $graph->getName(),
				'graph_id' => $graph->getId()
			) ) );
		}
		else {
			
			$bottom = $cluster->getNormalizedSortValue($graph->getId());
			if( !$collection->isSortAsc() ) {
				$bottom = 1-$bottom;
			}
			
			die( json_encode( array(
				'position' => $cluster->getSortIndex($graph->getId()),
				'bottom' => $bottom * 100,
				'cluster_id' => $cluster->getId(),
				'name' => $graph->getName(),
				'graph_id' => $graph->getId()
			) ) );
			
		}
	}
	
	
}

?>
