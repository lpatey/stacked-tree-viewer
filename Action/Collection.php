<?php

class Action_Collection extends Core_UserAction {

	private $_collection = array();
	
	public function execute() {
		
		if( isset($_POST['snapshot']) ) {
			$this->getUser()->setCurrentCollection( $this->getUser()->getCurrentCollection()->restore( $_POST['snapshot' ] ) );
		}
		
		if( $this->getUser()->hasCurrentCollection() ) {
			$this->getUser()->getCurrentCollection()->save();
		}
	
		if( isset( $_POST['folder'] ) ) {
			$collection = new Collection( $_POST['folder'] );
			$this->getUser()->setCurrentCollection( $collection->restore() );
		}
		
		$this->_collection = $this->getUser()->getCurrentCollection();
	}
	
	public function getCollection() {
		return $this->_collection;
	}

}

?>
