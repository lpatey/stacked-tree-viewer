<?php

class Action_GetSnapshotsDialog extends Core_CollectionAction {

	private $_snapshots;

	public function execute() {
	
		$this->_collection = $this->getCollection();
		$this->_snapshots = $this->_collection->getSnapshots();
	
	}

	
	public function hasSnapshots() {
		return $this->_snapshots;
	}
	
	public function getSnapshots() {
		return $this->_snapshots;
	}

}

?>
