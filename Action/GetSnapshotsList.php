<?php

class Action_GetSnapshotsList extends Core_CollectionAction {
	
	private $_snapshots;

	public function execute() {
	
		$collection = $this->getCollection();
		
		if( isset($_POST['snapshot' ]) ) {
			
			$snapshot = basename($_POST['snapshot' ]);

			$collection->save( $snapshot );
			
		}
		
		$this->_snapshots = $collection->getSnapshots();
	
	}

	
	public function hasSnapshots() {
		return $this->_snapshots;
	}
	
	public function getSnapshots() {
		return $this->_snapshots;
	}

}

?>
