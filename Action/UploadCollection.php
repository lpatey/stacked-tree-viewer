<?php

class Action_UploadCollection extends Core_UserAction {

	public function execute() {
		
		if( $_FILES ) {
			$this->_uploadFiles();
		}
		
	
	}
	
	private function _uploadFiles() {
	
		ignore_user_abort(true);
		set_time_limit(0);
		session_write_close();
	
		$name = $_FILES['source']['name'];

		if( $_POST['destination'] ) {
			$entity = new Group( $_POST['destination'] );
			if( !$entity->isAccessible() ) {
				$this->error( 'Cannot access to group' );
			}
		}
		else {
			$entity = $this->getUser();
		}
		
		$folder = pathinfo( $name, PATHINFO_FILENAME ) . date( ' Y-d-M H-m-s' );
		$path = $entity->getCollectionsDirectory()  . '/' . $folder . '/';
		
		mkdir( $path );
		
		move_uploaded_file( $_FILES['source']['tmp_name'], $path . '/' . $name );
		
		Clue_Kernel_Fs::writeIniFile( $path . '/collection.ini', array( 'name' => $name, 'snapshot' => Clue_Kernel_Conf::get( 'stv.snapshot.default', 'Default' ) ) );
		
		mkdir( $path . '/snapshots' );
		
		$java = Clue_Kernel_Conf::get( 'stv.java.directory' );
		
		$memory = Clue_Kernel_Conf::get( 'stv.java.memory' );
		
		$result = `java -Djava.awt.headless=true -jar -Xmx$memory $java/IPI.jar --input "$path/$name" --verbose --output "$path/collection" --matrix similarity --clustering --centrality --irln 2> "$path/collection.log"`;
		
		preg_match( '/Size : ([0-9]+)/', $result, $size );
		$size = $size[1];
		
		$infos = parse_ini_file( $path . '/collection.ini' );
		$infos['available'] = true;
		$infos['size'] = $size;
		Clue_Kernel_Fs::writeIniFile( $path . '/collection.ini', $infos );
		
		$collection = new Collection( $path );
		$collection->save();
		
	}
}

?>
