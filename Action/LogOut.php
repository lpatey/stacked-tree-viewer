<?php

class Action_LogOut extends Core_Action {

	public function execute() {
		unset( $_SESSION['user'] );
		header( 'Location: index.php' );
		exit(0);
	}
}
