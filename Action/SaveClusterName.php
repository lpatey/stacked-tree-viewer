<?php

class Action_SaveClusterName extends Core_CollectionAction {

	
	public function execute() {
			
		$cluster_id = $_POST['cluster_id'];
		$name = $_POST['name'];
		
		$collection = $this->getCollection();
		
		$cluster = $collection->getCluster( $cluster_id );
		$cluster->setName( $name );
		
		$path = $collection->getClusterPath();
		$content = file_get_contents( $path );
		$content = preg_replace( '/^((?:[^\n]+\n){' .( $cluster->getId() - 1 ). '} +[0-9\-]+ +[0-9\-]+ +[0-9.\-]+ +[0-9\-]+(?: +#......)?)([^\n]*)/', '$1 ' . $name, $content );
		
		file_put_contents( $path, $content );
		
		exit();
		
	}
	
}

?>
