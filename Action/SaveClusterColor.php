<?php

class Action_SaveClusterColor extends Core_CollectionAction {

	
	public function execute() {
			
		$cluster_id = $_POST['cluster_id'];
		$color = $_POST['color'];
		
		$collection = $this->getCollection();
		
		$cluster = $collection->getCluster( $cluster_id );
		
		if( $cluster ) {
			$cluster->setColor( $color );
		}
		
		$path = $collection->getClusterPath();
		$content = file_get_contents( $path );
		if( $color ) {
			$color = ' ' . $color;
		}
		$content = preg_replace( '/^((?:[^\n]+\n){' .( $cluster->getId() - 1 ). '} +[0-9\-]+ +[0-9\-]+ +[0-9.\-]+ +[0-9\-]+)( +#......)?([^\n]*)/', '$1' .$color . '$3', $content );
		
		file_put_contents( $path, $content );
		
		exit();
		
	}
	
}

?>
