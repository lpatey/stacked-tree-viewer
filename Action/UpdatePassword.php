<?php

class Action_UpdatePassword extends Core_UserAction {

	public function execute() {

		$user = $this->getUser();
		if( !$user->authenticate( $_POST['old_password'] ) ) {
			$this->error( 'Wrong password' );
		}

		if( !$_POST['password'] ) {
			$this->error( 'Password empty' );
		}

		if( $_POST['password'] != $_POST['confirmation'] ) {
			$this->error( 'Password and confirmation does not match' );
		}

		$user->setPassword( $_POST['password'] );
		$user->save();

		$this->success( 'Password changed' );
	}

}
