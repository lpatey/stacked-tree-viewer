<?php

class Action_UnselectCollection extends Core_UserAction {

	public function execute() {
		$this->getUser()->setCurrentCollection( null );
		header( 'Location: index.php' );
		exit(0);
	}

}
