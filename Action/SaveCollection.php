<?php

class Action_SaveCollection extends Core_UserAction {

	public function execute() {
		
		
		ignore_user_abort(true);
		set_time_limit(0);
		
		if( $this->getUser()->hasCurrentCollection() ) {
			$this->getUser()->getCurrentCollection()->save();
		}
		exit();
	
	}
	
}
	
?>
