<?php

class Action_GroupsList extends Core_UserAction {

	private $_groups;

	public function execute() {
		$this->_groups = $this->getUser()->getGroups();
	}

	public function getGroups() {
		return $this->_groups;
	}
}
