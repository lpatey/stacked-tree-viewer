<?php

class Action_GetGraphAt extends Core_CollectionAction {

	public function execute() {

		$position = $_GET['position'];
		$collection = $this->getCollection();
		$cluster = $collection->getCluster( $_GET['cluster_id']);
		$graphs = $cluster->getGraphs();
		$count = count($graphs);
		
		if( !$collection->isSortAsc() ) {
			$position = 1-$position;
		}
		
		if( $collection->getPositionPolicy() == $collection->POSITION_BY_INDEX ) {
			
			$position = round($count*$position);
			if( $position == $count ) {
				$position--;
			}
			$graph = $graphs[ $position ];
			
			$bottom = $cluster->getSortIndex( $graph->getId() )/$count;
			if( !$collection->isSortAsc() ) {
				$bottom = 1-$bottom;
			}
			
			die( json_encode( array(
				'position' => $cluster->getSortIndex( $graph->getId() ),
				'bottom' => $bottom * 100,
				'cluster_id' => $cluster->getId(),
				'name' => $graph->getName(),
				'graph_id' => $graph->getId()
			) ) );
		}
		else {
			
			$graph = $this->_searchGraphByDichotomy( $graphs, 0, $count-1, $position, $cluster );
		
			$bottom = $cluster->getNormalizedSortValue($graph->getId());

			if( !$collection->isSortAsc() ) {
				$bottom = 1-$bottom;
			}
			
			die( json_encode( array(
				'position' => $cluster->getSortIndex($graph->getId()),
				'bottom' => $bottom * 100,
				'cluster_id' => $cluster->getId(),
				'name' => $graph->getName(),
				'graph_id' => $graph->getId()
			) ) );
			
		}
	}
	
	private function _searchGraphByDichotomy( $graphs, $start, $end, $value, $cluster ) {
	
		$position = floor( ($start + $end) / 2 );

		if( $position == $start ) {
			return $value > ($cluster->getNormalizedSortValue($graphs[$end]->getId()) 
						+ $cluster->getNormalizedSortValue($graphs[$start]->getId()))/2 ? $graphs[$end] : $graphs[$start];
		}
		
		$graph = $graphs[ $position ];
		
		if( $cluster->getNormalizedSortValue($graph->getId()) > $value ) {
			return $this->_searchGraphByDichotomy( $graphs, $start, $position, $value, $cluster );
		}
		else {
			return $this->_searchGraphByDichotomy( $graphs, $position, $end, $value, $cluster );
		}
		
	}
	
}

?>
