<?php

class Clue_Kernel_Bean {

	private $__lazyCache = array();

	public function __call( $method, $args ) {
	
		if( Clue_Kernel_String::startsWith( $method, 'parent' ) ) {
			$parent = Clue_Kernel_String::lcFirst( substr( $method, strlen('parent') ) );
			return $this->__call( $parent, $args );
		}
	
		if( ($name = self::isLazy( $method ) ) !== false ) {
		
			$name = strtolower( $name[0] ) . substr( $name, 1 );
			$key = $name . serialize( $args );
			
			if( !isset($this->__lazyCache[ $key ] ) ) {
				$this->__lazyCache[ $key ] = call_user_func_array( array( $this, $name ), $args );
			}
			
			return $this->__lazyCache[ $key ];
		}
		else if( ($name = self::isGetter( $method ) ) !== false ) {
		
			$variable = Clue_Kernel_String::toUnderscores( $name );
					
			if( !isset( $this->$variable ) && Clue_Kernel_Name::isSingular( $variable ) && $args ) {
			
				$variable = Clue_Kernel_Name::toPlural( $variable );
				$value = $this->$variable;
				$value = $value[ $args[0] ];
			}
			else {
				$value = $this->$variable;
			}
		
			Clue_Kernel_Event::trigger( 'BeanGetter', $variable, $value );
		
			return $value;
		}
		else if( ( $name = self::isSetter( $method ) ) !== false ) {
		
			$variable = Clue_Kernel_String::toUnderscores( $name );
			
			Clue_Kernel_Event::trigger( 'BeanSetter', $variable, $args[0] );
			
			$this->$variable = $args[0];
			
			return $this;
		}
		else if( ( $name = self::isChecker( $method ) ) !== false ) {
		
			$variable = Clue_Kernel_String::toUnderscores( $name );
			
			Clue_Kernel_Event::trigger( 'BeanChecker', $variable, $this->$variable );
		
			$name = substr( $method, 2 );
			return in_array( $this->$variable, array('yes', 'true', 'oui') );
		}
		else if( ( $name = self::isAdder( $method ) ) !== false ) {
		
			$name = substr( $method, 3 );
			
			if( Clue_Kernel_String::endsWith( $name, 'Listener' ) ) {
				$listenerArray = '__' . strtolower($name[0] ) . substr( $name, 1 ) . 's';
				array_push( $this->$listenerArray, $args[0] );
			}
			
			return $this;
		}
		else if( ( $name = self::isHaver( $method ) ) !== false ) {
		
			$variable = Clue_Kernel_String::toUnderscores( $name );
	
			if( !isset( $this->$variable ) && Clue_Kernel_Name::isSingular( $variable ) && $args ) {
			
				$variable = Clue_Kernel_Name::toPlural( $variable );
				$value = $this->$variable;
				$value = isset( $value[ $args[0] ] ) &&  $value[ $args[0] ];
			}
			else {
				$value = isset( $this->$variable ) && $this->$variable;
			}
		
			Clue_Kernel_Event::trigger( 'BeanHaver', $variable, $value ? $this->$variable : null );
			
			return $value;
		
		}
		else if( ( $name = self::isRemover( $method ) ) !== false ) {

			$variable = Clue_Kernel_String::toUnderscores( $name );
			
			unset( $this->$variable );
			
			return $this;
		}
		else {
			throw new Clue_Kernel_Exception( 'Unknown method ' . $method );
		}

	
	}


	public static function isGetter( $method ) {
		foreach( Clue_Kernel_Conf::getArray( 'kernel.getters', array( 'get' ) ) as $getter ) {
			if( Clue_Kernel_String::startsWith( $method, $getter ) ) {
				return substr( $method, strlen($getter) );
			}
		}
		return false;
	}
	
	public static function isSetter( $method ) {
		foreach( Clue_Kernel_Conf::getArray( 'kernel.setters', array( 'set' ) ) as $setter ) {
			if( Clue_Kernel_String::startsWith( $method, $setter ) ) {
				return substr( $method, strlen($setter) );
			}
		}
		return false;
	}
	
	public static function isAdder( $method ) {
		foreach( Clue_Kernel_Conf::getArray( 'kernel.adders', array( 'add' ) ) as $adder ) {
			if( Clue_Kernel_String::startsWith( $method, $adder ) ) {
				return substr( $method, strlen($adder) );
			}
		}
		return false;
	}
	
	public static function isHaver( $method ) {
		foreach( Clue_Kernel_Conf::getArray( 'kernel.havers', array( 'has' ) ) as $haver ) {
			if( Clue_Kernel_String::startsWith( $method, $haver ) ) {
				return substr( $method, strlen($haver) );
			}
		}
		return false;
	}
	
	public static function isRemover( $method ) {
		foreach( Clue_Kernel_Conf::getArray( 'kernel.removers', array( 'remove' ) ) as $remover ) {
			if( Clue_Kernel_String::startsWith( $method, $remover ) ) {
				return substr( $method, strlen($remover) );
			}
		}
		return false;
	}
	
	public static function isLazy( $method ) {
		foreach( Clue_Kernel_Conf::getArray( 'kernel.lazys', array( 'lazy' ) ) as $lazy ) {
			if( Clue_Kernel_String::startsWith( $method, $lazy ) ) {
				return substr( $method, strlen($lazy) );
			}
		}
		return false;
	}
	
	public static function isChecker( $method ) {
		foreach( Clue_Kernel_Conf::getArray( 'kernel.checkers', array( 'is,check' ) ) as $checker ) {
			if( Clue_Kernel_String::startsWith( $method, $checker ) ) {
				return substr( $method, strlen($checker) );
			}
		}
		return false;
	}

	public function __destruct() {
		Clue_Kernel_Event::trigger( 'BeanDestructed' );
		$this->__lazyCache = array();
	}
	
	public function __toString() {
		ob_start();
		print_r( $this );
		return ob_get_clean();
	}

	protected function getFunctionArgs() {
		$args = func_get_args();
		$arguments = array_shift( $args );
		$return = array();
		foreach( $args as $i => $arg ) {
		
			if( isset( $arg[0] ) ) {
				$arg['if'] = $arg[0];
			}
			
			if( isset( $arg[1] ) ) {
				$arg['default'] = $arg[1];
			}
		
			if( isset( $arg['if'] ) ) {
				foreach( $arguments as $argument ) {
					if( call_user_func_array( $arg['if'], array( $argument ) ) ) {
						$return[$i] = $argument;
					}
				}
				if( !isset($return[$i]) && isset($arg['default']) ) {
					$return[$i] = $arg['default'];
				}
			}
		}
		ksort($return);
		return $return;
	}
}

?>