<?php

class Clue_Kernel_Php {

	public static function getConstValue( $const, $default = '' ) {

		if( defined( $const ) ) {
			return constant($const);
		}
		
		return $default;
	}
	
	public static function init( &$var, $default ) {
		if( !$var ) {
			$var = $default;
		}
	}
}

?>