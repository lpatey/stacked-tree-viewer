<?php Clue_Kernel_Engine::printRessource( 'benchmark.css' ); ?>
<?php Clue_Kernel_Engine::printRessource( 'benchmark.js' ); ?>
<div id="phpcore_benchmark">
<div id="bench_transparent"></div>
<div class="bench_container" onclick="phpcore_toogle_benchmark(this)">
<?php
list( $time, $count, $html ) = self::getHTML( self::$bench );
?>
<div class="bench_title">Benchmark (<?php echo ceil( $time*1000 ) ?> ms)</div>
<div>
<?php echo $html; ?>
</div>
</div>
</div>