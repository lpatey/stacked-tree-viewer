<?php

class Clue_Kernel_Name {

	public static function toPlural( $name ) {
		if( Clue_Kernel_String::endsWith( $name, 'ux' ) ) {
			return $name;
		}	

		if( Clue_Kernel_String::endsWith( $name, 'y' ) ) {
			$name = substr( $name, 0, -1 ) . 'ies';
		}
		else if( Clue_Kernel_String::endsWith( $name, 'eu' ) || Clue_Kernel_String::endsWith( $name, 'au' ) ) {
			$name .= 'x';
		}
		else if( Clue_Kernel_String::endsWith( $name, 'ss' ) ) {
			$name .= 'es';
		}
		else if( !Clue_Kernel_String::endsWith( $name, 's' ) ) {
			$name .= 's';
		}
		
		return $name;
	}
	
	public static function toSingular( $name ) {

		if( Clue_Kernel_String::endsWith( $name, 'ies' ) ) {
			$name = substr( $name, 0, -3 ) . 'y';
		}
		else if( Clue_Kernel_String::endsWith( $name, 's' )
			&& !Clue_Kernel_String::endsWith( $name, 'ss' ) ) {
			$name = substr( $name, 0, -1 );
		}
		else if( Clue_Kernel_String::endsWith( $name, 'x' ) ) {
			$name = substr( $name, 0, -1 );
		}
		
		return $name;
		
	}

	public static function toAbsSingular( $name ) {

		if( self::isPlural( $name ) ) {
			return self::toSingular( $name );
		}
		else {
			return $name;
		}
	}

	public static function toAbsPlural( $name ) {

		if( self::isSingular( $name ) ) {
			return self::toPlural( $name );
		}
		else {
			return $name;
		}
	}

	public static function isPlural( $name ) {

		return $name == self::toPlural( $name );
	}

	public static function isSingular( $name ) {

		return !self::isPlural( $name );
	}

}

?>
