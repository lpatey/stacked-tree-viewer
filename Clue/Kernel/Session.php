<?php

class Clue_Kernel_Session {

	public static function & get() {
	
		if( !isset( $_SESSION ) ) {
			session_start();
		}
		
		$args = func_get_args();
		
		$ses = &$_SESSION['local'];
		
		foreach( $args as $arg ) {
	
			$ses = &$ses[ $arg ];
		}
		
		return $ses;
	
	}

}

?>