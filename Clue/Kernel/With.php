<?php

class Clue_Kernel_With {

	private $object;
	private $args;
	private $left;
	
	public function __construct( $object ) {
	
		$this->object = $object;
		$args = func_get_args();
		array_shift( $args );
		$this->args = $args;
		
		$this->left = Clue_Kernel_String::startsWith( get_class( $this ), 'Left' );
		
	}
	
	public function __call( $name, $args ) {
	
		if( $this->left ) {
			$args = array_merge( $this->args, $args );
		}
		else {
			$args = array_merge( $args, $this->args );
		}
	
		return call_user_func_array( array( $this->object, $name ), $args );
		
	}

}

?>