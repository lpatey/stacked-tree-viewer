<?php

abstract class Clue_Kernel_Module {

	public static function getVersion() {
	}
	
	public static function getModuleName() {
	}
	
	public static function getDependencies() {
		return array( 'phpker' );
	}
	
	public static function beforeInit() {
	}
	
	public static function init() {
	}
	
	public static function afterInit() {
	}

}

?>