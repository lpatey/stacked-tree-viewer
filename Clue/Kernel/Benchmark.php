<?php

class Clue_Kernel_Benchmark {

	private static $bench = array();
	private static $called = false;
	
	public static function mark() {
	
		$args = func_get_args();
		
		$data = &self::$bench;

		foreach( $args as $arg ) {
		
			$data = &$data[$arg];
		}
		
		
		$data[] = microtime( true );
		
		
	}
	
	public static function hasBeenCalled() {
		return self::$called;
	}
	
	public static function getHTML( $bench ) {

		if( isset($bench[0]) ) {
		
			$total = 0;
			while( $bench ) {
				$start = array_shift( $bench );
				$end = array_shift( $bench );
				$total += $end - $start;
			}
			
			return array( $total, 1, '' );
		}
		else {
			
			$total = 0;
			$number = 0;
			$return = '';
		
			$return .= '<ul>';
			foreach( $bench as $key => $value ) {
		
				list( $delay, $count, $html ) = self::getHTML( $value );
				if( $html ) {
					$return .= '<li>' . $key . ' ( ' . $count . ' ) ' . ceil($delay*1000) . ' ms' . "\n";
					$return .= $html . '</li>';
				}
				else {
					$return .= '<li>' . $key . ' ' . ceil($delay*1000) . ' ms</li>';
				}
				
				$total += $delay;
				$number += $count;
				
			}
			$return .= '</ul>';
		
		}
		
		return array( $total, $number, $return );
	}
	
	public static function show( $now = false ) {
	
		if( !$now ) {
			return self::$called = true;
		}
		
		require_once 'res/benchmark.html.php';
	}
	
	public static function onShutDown( $output ) {
	
		if( (Clue_Kernel_Conf::get( 'kernel.debug', false ) 
			|| self::hasBeenCalled()) ) {
			ob_start();
			self::show( true );
			$output = preg_replace( '/<\/body>/i', ob_get_clean() . '$0', $output );
		}
		
		return $output;
	}

}

?>