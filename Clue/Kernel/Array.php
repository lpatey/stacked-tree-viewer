<?php

class Clue_Kernel_Array {

	public static function toArray( $a ) {
		return is_array( $a ) ? $a : array( $a );
	}
	
	public static function key( $array, $k, $v = null ) {
		return isset( $array[$k] ) ? $array[$k] : $v;
	}
	
	public static function alias( & $array, $key, $alias ) {
		if( isset( $array[ $key ] ) ) {
			$array[ $alias ] = $array[ $key ];
		}
		else if( isset( $array[ $alias ] ) ) {
			$array[ $key ] = $array[ $alias ];
		}
	}
}

?>