<?php

class Clue_Kernel_Class {

	public static function isChildOf( $class, $ancestor ) {
	
		while( $class = get_parent_class( $class ) ) {
		
			if( $class == $ancestor ) {
				return true;
			}
		}
		return false;
	}
	
	public static function toAbsoluteName( $name ) {
		return is_object($name) ? get_class( $name ) : $name;
	}


	public static function getSuperClass( $string ) {

		if( is_object( $string ) ) {
			$string = get_class( $string );
		}

		return preg_replace( '/.*([A-Z][a-z]+)/', '$1', $string );
	}
	
	public static function isInstanceOf( $class, $parent ) {
	
		if( is_string($class) ) {
			return class_exists( $class, false ) && in_array( $parent, class_parents( $class ) );
		}
		
		if( !is_object($class) ) {
			return false;
		}
	
		if( method_exists( $class, 'isInstanceOf' ) ) {
			return $class->isInstanceOf( $parent );
		}
		
		return $class instanceof $parent;
	}

	public static function getChildClass( $string ) {

		if( is_object( $string ) ) {
			$string = get_class( $string );
		}

		return preg_replace( '/(.*)[A-Z][a-z]+/', '$1', $string );
	}
	
	public static function getRealClass( $mixed ) {
	
		if( is_object( $mixed ) ) {
			$mixed = get_class( $mixed );
		}
		
		return substr( $mixed, strrpos( '_', $mixed ) );
	}
	
	public static function isDynamicMethod( $o, $method ) {
	
		return !method_exists( $o, $method );
	}


}

?>