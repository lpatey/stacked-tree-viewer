<?php

class Clue_Kernel_String {

	public static function toUnderscores( $name ) {
		
		$name = preg_replace( '/[A-Z]/e', 'strtolower("_$0")', $name );

		if( $name[0] == '_' ) {
			$name = substr( $name, 1 );
		}
		
		return $name;
	}
	
	public static function toCamelCaps( $name ) {
		return preg_replace( '/(?:_|^)(.)/e', 'strtoupper( "$1" )', $name );
	}
	
	public static function startsWith( $string, $search ) {

		return substr( $string, 0, strlen( $search ) ) == $search;
	}

	public static function endsWith( $string, $search ) {

		return substr( $string, - strlen( $search ) ) == $search;
	}
	
	public static function lcFirst( $string ) {
		return $string ? mb_strtolower( $string[0] ) . substr( $string,1 ) : $string;
	}
	
	public static function ucFirst( $string ) {
		return ucfirst( $string );
	}

}

?>