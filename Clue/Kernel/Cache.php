<?php

class Clue_Kernel_Cache {

	private $module;
	private $path;
	
	public function __construct( $module ) {
		
		$this->module = $module;
		$args = func_get_args();
		
		$cache = Clue_Kernel_Conf::get( 'kernel.cache.directory', 
			Clue_Kernel_Fs::path( dirname(__FILE__), 'CACHE' ) );

		array_unshift( $args, $cache );
		$cache = Clue_Kernel_Fs::path( $args );
		
		if( !is_dir( $cache ) ) {
			mkdir( $cache, 0777, true );
		}
		
		$this->path = $cache;
	}
	
	public function checkValidity( $lastModification ) {
	
		if( time() - @filemtime( $this->path ) > $lastModification ) {
			
			
			$dh = opendir( $this->path );
			
			while( $item = readdir( $dh ) ) {
			
				if( $item[0] != '.' ) {
				
					unlink( Clue_Kernel_Fs::path( $this->path, $item ) );
				}
			}
		}
	}
	
	
	
	/*
	* File cache : Put files in cache
	*/

	
	public function getFile( $name ) {
	
		if( !$this->hasFile( $name ) 
			|| @file_get_contents( $this->getFilePath( $name . '.md5' ) ) != md5( file_get_contents( $name, true ) ) ) {
		
			return '';
		}
		
		return @$this->getFilePath( $name );
	}
	

	public function hasFile( $name ) {
	
		return file_exists( $this->getFilePath( $name ) );
	}
	
	public function setFile( $name, $content ) {
	
		file_put_contents( $this->getFilePath( $name . '.md5' ), md5( file_get_contents( $name, true ) ) );
		file_put_contents( $this->getFilePath( $name ), $content );
	}
	
	
	private function getFilePath( $name ) {
	
		return Clue_Kernel_Fs::path( $this->path, md5($name) . '.tmp' );
	}
	
	
	
	/*
	* Var cache : Put vars in cache
	*/
	
	
	public function getVar( $name ) {
	
		return $this->hasVar( $name ) ? unserialize(  file_get_contents( $this->getVarPath( $name ) )  ) : null;
	}
	

	public function hasVar( $name ) {
	
		return file_exists( $this->getVarPath( $name ) );
	}
	
	public function setVar( $name, $value ) {
	
		file_put_contents( $this->getVarPath( $name ), serialize( $value ) );
	}
	
	
	private function getVarPath( $name ) {
	
		return Clue_Kernel_Fs::path( $this->path, $name . '.srl' );
	}
	
}

?>