<?php

class Clue_Kernel_Event {

	private static $eventListeners;

	public static function addListener( $event, $listener ) {
	
		$args = func_get_args();
		array_shift( $args );
		array_shift( $args );
	
		self::$eventListeners[ $event ][] = array( 
			'listener' => $listener, 
			'args' => $args );
	}
	
	public static function removeListener( $event, $listener ) {
	
		$args = func_get_args();
		array_shift( $args );
		array_shift( $args );
		
		$listeners = self::$eventListeners[ $event ];
		$listeners = $listeners ? $listeners : array();
	
		foreach( $listeners as $i => $l ) {
		
			if( $l['listener'] == $listener ) {
				unset( self::$eventListeners[ $event ][ $i ] );
			}
			
		}
	}
	
	public static function triggerReduce( $event, $data ) {
		
		$listeners = @self::$eventListeners[ $event ];
		$listeners = $listeners ? $listeners : array();
		
		foreach( $listeners as $listener ) {
		
			$data = call_user_func_array( $listener['listener'], 
			array_merge( array( $data ), $listener['args'] ) );
		}
		
		return $data;
	
	}
	
	public static function trigger( $event ) {
	
		$args = func_get_args();
		array_shift( $args );
		
		@$listeners = self::$eventListeners[ $event ];
		$listeners = $listeners ? $listeners : array();
		
		$results = array();
		
		foreach( $listeners as $listener ) {
		
			$results[] = call_user_func_array( $listener['listener'], 
			array_merge( $args, $listener['args'] ) );
		}
		
		return $results;
	
	}

}

?>