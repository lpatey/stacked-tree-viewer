<?php

class Clue_Kernel_Loader {

	private static $includePaths = array();
	private static $superClasses = array();
	private static $dynamicClasses = array();
	private static $filters = array();

	public static function addSuperClass( $suffix, $class ) {

		self::$superClasses[ $suffix ] = $class;
	}

	public static function getSuperClasses() {
	
		return self::$superClasses;
	}
	
	
	private static function defaultFilter( $name ) {
	
		return str_replace( '_', DIRECTORY_SEPARATOR, $name ) . '.php';
	}
	
	public static function addFilter( $filter ) {
		self::$filters[] = $filter;
	}
	
	public static function getFilters() {
		return self::$filters;
	}
	

	public static function loadIncludePath() {
	
		if( self::$includePaths ) {
			return;
		}
		self::$includePaths = explode( PATH_SEPARATOR, get_include_path() );
	}

	public static function addIncludePath() {
	
		self::loadIncludePath();

		$args = func_get_args();
		$path = realpath( call_user_func_array( array( 'Clue_Kernel_Fs', 'path' ), $args ) );
		
		if( !in_array( $path, self::$includePaths ) ) {
			self::$includePaths[] = $path;
		}
		
		return set_include_path( implode( PATH_SEPARATOR, self::$includePaths ) );
	}

	public static function autoload( $name, $dirs = array() ) {
		
		foreach( self::getFilters() as $filter ) {
		
			$results = call_user_func( $filter, $name );
			
			if( !is_array( $results ) ) {
				$results = array( $results );
			}

			foreach( $results as $result ) {
				
				if( Clue_Kernel_Fs::fileExists( $result ) ) {
					require_once $result;
					if( class_exists( $name ) || interface_exists( $name ) ) {
						return;
					}
				}
			}
		
		}
		
		$super = '';

		foreach( self::getSuperClasses() as $suffix => $class ) {
		
			if( Clue_Kernel_String::endsWith( $name, $suffix ) ) {
				$super = $class;
			}
		
		}
		
		if( !$super ) {
			$super = Clue_Kernel_Event::triggerReduce( 'DefaultParent', $name );
		}

		self::$dynamicClasses[] = $name;

        eval( 'class ' . $name . ' extends ' . $super . ' { 
            public static function fetch() { 
                $args = func_get_args();
                return self::fetchStatic( \'' . $name . '\', $args ); 
            } 
        }' );
	}
	
	public static function getDynamicClasses() {
	
		return self::$dynamicClasses;
	}

	
}

?>