<?php

class Clue_Kernel_Fs {

	public static function path() {
		$args = func_get_args();
		if( is_array( $args[0] ) ) {
			$args = $args[0];
		}
		
		foreach( $args as $i => $arg ) {
			if( !$arg ) {
				unset( $args[$i] );
			}
		}
		return implode( DIRECTORY_SEPARATOR, $args );
	}

	public static function fileExists( $file ) {
	
		$args = func_get_args();
		$file = call_user_func_array( array( 'self', 'path' ), $args );
		
		set_error_handler( array('self', 'path') , E_ALL);
   		$fh = @fopen( $file, 'r', true );
		restore_error_handler();
		
		if( !$fh ) {
			return false;
		}
		
		fclose( $fh );
		
		return true;
	}

	public static function includeFile( $file ) {
	
		$args = func_get_args();
		$file = call_user_func_array( array( 'self', 'path' ), $args );

		include $file;
	}
	
	public static function fileGetContents( $file ) {
	
		$args = func_get_args();
		$file = call_user_func_array( array( 'self', 'path' ), $args );

		return file_get_contents( $file, true );

	}
	
	public static function writeIniFile($path, $assoc_array) {
 
 
      $content = '';
	    foreach ($assoc_array as $key => $item) {
	        if (is_array($item)) {
	            $content .= "\n[$key]\n";
	            foreach ($item as $key2 => $item2) {
	                $content .= "$key2 = \"$item2\"\n";
	            }       
	        } else {
	        	if( is_bool( $item ) ) {
	        		$content .= "$key = " . ($item ? 'true' : 'false') . "\n";
	        	}
	        	else if( is_numeric( $item ) ) {
	           		$content .= "$key = $item\n";
	        	}
	        	else {
	        		 $content .= "$key = \"$item\"\n";
	        	}
	        }
	    }       
	   
	    if (!$handle = fopen($path, 'w')) {
	        return false;
	    }
	    
	    if (!fwrite($handle, $content)) {
	        return false;
	    }
	    
	    fclose($handle);
	    return true;
	}
	

}

?>
