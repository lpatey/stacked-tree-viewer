<?php

class Clue_Kernel_Engine {

	private static $request;
	private static $directory;
	
	public static function getVersion() {
		return '0.1.4';
	}
	
	public static function getModuleName() {
		return 'phpker';
	}
	
	public static function getDependencies() {
		return array();
	}
	
	public static function getRequest() {
		
		if( !self::$request ) {
			self::$request = new Clue_Kernel_Request();
			if( !self::$request->class ) {
				self::$request->class = 'user.Home';
			}

			self::$request->class = str_replace( '.' , '_', self::$request->class );
		}
		
		return self::$request;
		
	}
	
	public static function initAll( $config = null ) {
	
		self::init( $config );
		
		$sep = DIRECTORY_SEPARATOR;
		
		$path = Clue_Kernel_Fs::path(dirname(__FILE__), '..' );
		
		$dh = opendir( $path );
		
		while( $dir = readdir( $dh ) ) {
		
			if( $dir[0] == '.' || $dir == 'Kernel' ) {
				continue;
			}
		
			if( is_dir( $path . $sep . $dir ) ) {
			
				require_once Clue_Kernel_Fs::path( $path, $dir, 'Engine.php' );
				
				$class = 'Clue_' . $dir . '_Engine';
				
				call_user_func_array( array( $class, 'init' ), array() );
			}
		}
	}
	
	public static function init( $config = null ) {
	
		set_include_path( get_include_path() . PATH_SEPARATOR . dirname( __FILE__ ) );

		ob_start();
		
		self::$directory = getcwd();
	
		register_shutdown_function( array( get_class(), 'shutdown' ) );
		
		//mb_internal_encoding( 'UTF-8' );
		
		require_once 'Loader.php';
		require_once 'Fs.php';
		require_once 'Event.php';
		
		Clue_Kernel_Loader::addIncludePath( dirname( __FILE__ ) . '/../../' );
		
		Clue_Kernel_Loader::addFilter( array( 'Clue_Kernel_Loader', 'defaultFilter' ) );
	
		spl_autoload_register( 'Clue_Kernel_Loader::autoload' );
		
		Clue_Kernel_Benchmark::mark( 'Application', 'Clue', 'Initialization' );
		
		Clue_Kernel_Conf::load( $config );
		
		//Clue_Kernel_Loader::addIncludePath( dirname( __FILE__ ) . '/../' );

		Clue_Kernel_Loader::addSuperClass( 'With', 'Clue_Kernel_' );
	
		Clue_Kernel_Event::addListener( 'ShutDown', array( 'Clue_Kernel_Benchmark', 'onShutDown' ) );
		
		Clue_Kernel_Benchmark::mark( 'Application', 'Clue', 'Initialization' );
		
		if( self::hasRequest() ) {

			self::request();

			exit();
		}


	}

	public static function hasRequest() {
		return @$_GET['module'] == self::getModuleName() && $_GET['action'];
	}
	
	public static function request() {
	
		switch( $_GET[ 'action' ] ) {
		
			case 'ressource' :
				if( substr_count( $_GET['ressource'], '..' ) ) {
					return;
				}
				if( Clue_Kernel_String::endsWith( $_GET['ressource'], 'js' ) ) {
					header( 'Content-type: text/javascript' );
				}
				else if( Clue_Kernel_String::endsWith( $_GET['ressource'], 'css' ) ) {
					header( 'Content-type: text/css' );
				}
				require_once 'res/' . $_GET['ressource'];
				break;
		}
	}
	
	public static function printRessource( $res ) {
	
		if( Clue_Kernel_String::endsWith( $res, 'js' ) ) {
			echo '<script type="text/javascript" src="index.php?module=' 
			. self::getModuleName() . '&action=ressource&ressource=' . $res . '"></script>' . "\n";
		}
		else if( Clue_Kernel_String::endsWith( $res, 'css' ) ) {
			echo '<link rel="Stylesheet" type="text/css" href="index.php?module=' 
			. self::getModuleName() . '&action=ressource&ressource=' . $res . '" />' . "\n";
		}
	}
	
	public static function shutdown() {

		chdir( self::$directory );
		echo Clue_Kernel_Event::triggerReduce( 'ShutDown', ob_get_clean() );
		
	}
}

?>
