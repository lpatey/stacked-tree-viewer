<?php

class Clue_Kernel_Conf {

	private static $conf = array();
	private static $loaded = false;
	
	public static function load( $config = null ) {
	
		if( !$config ) {
			$config = 'Engine.ini';
		}
	
		if( !self::$loaded ) {
	
			self::$loaded = true;
			self::$conf = parse_ini_file( $config );
			Clue_Kernel_Event::trigger( 'ConfLoaded', self::$loaded );
			
		}
	}
	
	public static function get( $key, $default = '' ) {

		self::load();
		return isset( self::$conf[ $key ] ) ? 
			self::$conf[ $key ] : $default;
	}
	
	public static function set( $key, $value ) {
		if( is_array( $value ) ) {
			$value = implode( ',', $value );
		}
		self::$conf[ $key ] = $value;
	}
	
	public static function getArray( $key, $value ) {
		self::load();
		return isset( self::$conf[ $key ] ) ? 
			explode( ',', self::$conf[ $key ] ) : $value;
	}
	
	public static function push( $key, $value ) {
		$previous = self::get( $key );
		$previous .= ',' . $value;
		self::set( $key, $previous );
	}


}

?>
