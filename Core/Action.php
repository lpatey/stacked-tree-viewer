<?php

abstract class Core_Action {
	
	private $_view = null;
	
	protected function setView( $view ) {
		$this->_view = $view;	
	}

	public abstract function execute();
	
	public function isViewEnabled() {
		return true;
	}

	public function error( $message ) {
		throw new Core_CatchableException( array(
			'error' => true,
			'message' => $message
		) );
	}

	public function success( $message ) {
		throw new Core_CatchableException( array(
			'success' => true,
			'message' => $message
		) );
	}
	
	protected function getView() {
		if( $this->_view ) {
			return $this->_view;
		}
		$path = str_replace( '_', '/', get_class( $this ) );
		return $path . '.php';
	}
	
	public function before() {
		return true;
	}
	
	public function action( $name ) {
		
		$action = 'Action_' . $name;

		$o = new $action();
		
		ob_start();
		if( $o->before() ) {
			$o->execute();
			$o->after();
		}
		return ob_get_clean();
	}
	
	public function after() {
		if( $this->isViewEnabled() ) {
			$view = $this->getView();
			require 'View/' . $view;
		}
	}
}

?>
