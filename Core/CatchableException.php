<?php

class Core_CatchableException extends Core_Exception {

	private $_data;

	public function __construct( $data ) {
		parent::__construct( isset($data['message']) ? $data['message'] : 'Catchable exception' );
		$this->_data = $data;
	}

	public function getData() {
		return $this->_data;
	}

	public function __toString() {
		return json_encode( $this->_data );
	}


}
