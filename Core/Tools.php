<?php

class Core_Tools {

	public static function truncate( $string, $maxLength ) {
		return mb_strlen($string) > $maxLength ? mb_substr( $string, 0, $maxLength - 3 ) . '...' : $string;
	}
	
	public static function strtotime( $string ) {
		$string = str_ireplace( 
			array( 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche', 'janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre' ),
			array( 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday',  'January', 'February', 'March', 'April', 'May', 'Juny', 'July', 'August', 'September', 'October', 'November', 'December' ), $string );
		return strtotime( $string );
	}
}

?>