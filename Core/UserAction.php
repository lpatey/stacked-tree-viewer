<?php

abstract class Core_UserAction extends Core_Action {

	public function before() {

		if( !isset($_SESSION['user']) ) {
			$this->error( 'Authentication required' );
		}

		return true;
	}

	public function getUser() {
		return $_SESSION['user'];
	}
}

?>
