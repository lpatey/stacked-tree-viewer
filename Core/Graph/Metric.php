<?php

class Core_Graph_Metric {
	
	private $_count;
	private $_distance;
	private $_increment;
	private $_min;
	private $_max;
	private $_height;
	
	
	public function __construct( $min, $max, $height, $maxLineCount = 7 ) {
		
		$this->_min = $min;
		$this->_max = $max;
		$this->_height = $height;
		$dim = $max - $min;

		$this->_count = min($height/2, $maxLineCount );
		
		if( $dim <= 1 ) {
			$this->_increment = $dim/($this->_count);
		}
		else {
			$this->_increment = floor($dim/($this->_count));	
		}

		$this->_distance = max(2, $this->_increment*$height/$dim);
	}
	
	public function getCount() {
		return $this->_count;
	}
	
	public function getDistance() {
		return $this->_distance;
	}
	
	public function getIncrement() {
		return $this->_increment;
	}
	
	public function getMin() {
		return $this->_min;
	}
	
	public function getMax() {
		return $this->_max;
	}
	
	public function getHeight() {
		return $this->_height;
	}
	
	public function getValue( $i ) {
		return $this->getMin() + $this->getIncrement() * $i;
	}
	
}

?>