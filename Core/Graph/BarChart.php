<?php

class Core_Graph_BarChart extends Core_View_Component {
	
	private $_collection = null;
	private $_clusters = null;
	private $_title;
	private $_data;
	private $_positionPolicy;
	
	private $_barMetric;
	private $_hierarchyMetric;

	public function __construct( $collection ) {
		$this->_collection = $collection;
		$this->_compute();
	}
	
	public function getData() {
		return $this->_data;
	}
	
	public function getPositionPolicy() {
		return $this->getCollection()->getPositionPolicy();
	}
	
	public function getClustersNumber() {
		return count($this->_clusters);
	}
	
	public function getCluster( $id ) {
		return $this->_clusters[ $id ];
	}
	
	public function getClusters() {
		return $this->_clusters;
	}
	
	public function getCollection() {
		return $this->_collection;
	}
	
	public function getRoot() {
		return $this->_root;
	}
	
	public function getTitle() {
		return $this->_title;
	}
	
	public function getBarLeft() {
		return 89/($this->getClustersNumber());
	}
	
	public function getBarMetric() {
		return $this->_barMetric;
	}
	
	public function getHierarchyMetric() {
		return $this->_hierarchyMetric;
	}
	
	public function hasHierarchyMetric() {
		return $this->getCollection()->getClusterMode() == $this->getCollection()->HEIGHT_PROPORTIONAL;
	}
	
	public function isInverseXorByValue() {
		return ($this->getPositionPolicy() == $this->getCollection()->POSITION_BY_VALUE)
		  == $this->getCollection()->isSortAsc();
	}
	
	private function _compute() {
	
		$clusters = $this->getCollection()->clusterize();

		$this->_data[ 'ParentRoot' ] = $this->getCollection()->getParentRoot();
		
		$max = 0;
		foreach( $clusters as $cluster ) {
			$max = max($cluster->getSize(), $max );
		}
		
		$allocatedHeight = $this->getCollection()->getAllocatedHeight();
		$this->_barMetric = new Core_Graph_Metric( 0, $max, $allocatedHeight );
		if( $this->hasHierarchyMetric() ) {
			$min = $this->getCollection()->getMaxCluster()->getDistance();
			$max = $this->getCollection()->getRootCluster()->getDistance();
			$this->_hierarchyMetric = new Core_Graph_Metric( $min, $max, 85 - $allocatedHeight,
					min( 7, $this->getCollection()->getRootCluster()->getNotProportionalHeadTop() )
				);
		}

		$this->_clusters = $clusters;
		
		
		$this->_title = 'Size : ' . $this->getCollection()->getRootSize();
	}
	
	protected function getView() {
		return 'BarChart.php';
	}

}

?>