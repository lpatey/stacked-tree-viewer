<?php

abstract class Core_CollectionAction extends Core_UserAction {

	public function getCollection() {
		return $this->getUser()->getCurrentCollection();
	}
}
