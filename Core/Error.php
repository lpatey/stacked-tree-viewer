<?php
class Core_Error {

    static public function  handler($errno, $errstr, $errfile, $errline, $errcontext)
    {
        Core_Log::log($errstr);
        $stdError = Clue_Kernel_Conf::get('kernel.debug', false) ? $errstr : 'Service temporarily unavailable.';
        switch ($errno) {
            case E_USER_ERROR:
            case E_USER_WARNING:
                echo '<p class="Error">'.$errstr.'</p>';
				exit();
                break;
            case E_WARNING:
            case E_ERROR:
                echo '<p class="Error">'.$stdError.'</p>';
				exit();
                break;
            default:
                break;
        }

        return true;

    }

    
}
