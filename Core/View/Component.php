<?php

class Core_View_Component {

	protected function getView() {
		$name = str_replace( 'Core_View_', '', get_class( $this ) );
		$path = str_replace( '_', '/', $name );
		return $path . '.php';
	}

	public function __toString() {
		$view = $this->getView();
		ob_start();
		try {
			require 'View/Component/' . $view;
		}
		catch( Exception $e ) {
			echo $e;
		}
		return ob_get_clean();
	}

}

?>