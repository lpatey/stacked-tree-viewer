<?php

class Core_View_Box extends Core_View_Component {

	private $_title = '';
	private $_views = array();
	private $_selectedView = null;
	private $_context = null;
	private $_folded = false;
	
	public function __construct( $context, $title ) {
		$this->_context = $context;
		$this->_title = $title;
	}
	
	public function getTitle() {
		return $this->_title;
	}
	
	public function getContext() {
		return $this->_context;
	}
	
	public function getViews() {
		return $this->_views;
	}
	
	public function setFolded( $flag ) {
		$this->_folded = $flag;
	}
	
	public function isFolded() {
		return $this->_folded;
	}
	
	public function setSelectedView( $view ) {
		$this->_selectedView = $view;
	}
	
	public function getSelectedView() {
		return $this->_selectedView;
	}
	
	public function add( $key, $title ) {
		$this->_views[ $key ] = $title;
		if( !$this->_selectedView ) {
			$this->_selectedView = $key;
		}
	}

	protected function getView() {
		return 'Box.php';
	}

}

?>