<?php

class Core_Application {

	private static $_widgets = null;

	public static function getUser() {
		return @$_SESSION['user'];
	}
	
	public static function getWidgets() {
	
		if( !self::$_widgets ) {
			if( Clue_Kernel_Conf::get('kernel.debug', false) ) {
				self::$_widgets = new Widgets();
			}
			else {
				self::$_widgets = new Widgets( array( 'status' => 'prod' ) );
			}
		}
		return self::$_widgets;
	}
	
}

?>