<?php

class Core_Controller {

	public static function dispatch() {


        try {
            session_start();

            if( !isset( $_GET['action'] ) ) {
                $_GET['action'] = 'Index';
            }

            $action = 'Action_' . preg_replace( '/[^A-Za-z_]/', '', $_GET['action'] );

            $file = str_replace( '_', '/', $action ) . '.php';

            if( !file_exists( $file ) ) {
                throw new Exception( 'File not found ' . $file );
            }

            require_once $file;

            if( !in_array( 'Core_Action', class_parents( $action ) ) ) {
                throw new Exception( 'Invalid action ' . $action );
            }

            $o = new $action();

            if( $o->before() ) {
                $o->execute();
                $o->after();
            }
        } catch (Exception $e) {

				if( $e instanceof Core_CatchableException ) {
					echo $e;
					exit(0);
				}
            else if ($e instanceof Core_Exception || Clue_Kernel_Conf::get('kernel.debug', false)) {
                trigger_error( $e->getMessage(), E_USER_ERROR );
            } else {
                trigger_error( 'Service temporarily unavailable', E_USER_ERROR );
            }
            
        }
	
	}
}

?>
