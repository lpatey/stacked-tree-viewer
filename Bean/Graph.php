<?php

class Graph {

	private $_id;
	private $_name;
	private $_serialization;
	private $_collection;
	private $_node;
	private $_properties;
	private $_computed = false;
	private $_cluster;
	
	public function __construct( $id, $name, $node, $collection ) {
		$this->_id = $id;
		$this->_name = $name;
		$this->_node = $node;
		$this->_collection = $collection;
	}
	
	public function getId() {
		return $this->_id;
	}
	
	public function computes() {
		
		if( $this->_computed  ) {
			return $this;
		}
		$this->_computed = true;
		$serial = '';
			
		$doc = $this->_node->ownerDocument;
		$xpath = new DOMXPath( $doc );
		
		$vertices = $xpath->evaluate( 'vertices/*', $this->_node );
		$n_vertices = $vertices->length;
		
		$edges = $xpath->evaluate( 'edges/*', $this->_node );
		$n_edges = $edges->length;
		
		$serial = "\n\n\n";
		$serial .= sprintf( "%3d%3d\n", $n_vertices, $n_edges );
		
		$ids = array();
		$i=1;
		foreach( $vertices as $vertex ) {
			$ids[ $vertex->getAttribute( 'id' ) ] = $i++;
			$content = substr( $vertex->textContent, 1, -1 );
			$serial .= sprintf( "    0.0000    0.0000    0.0000 %-2s  0  0  0  0  0  0  0  0  0  0  0  0\n", $content );
		}
		
		foreach( $edges as $edge ) {
		
			$content = substr( $edge->childNodes->item(0)->textContent, 1, -1 );
			$atoms = explode( ',', $content );
			$properties = substr( $edge->childNodes->item(1)->textContent, 1, -1 );
			$serial .= sprintf( "%3d%3d%3d  0  0  0  0\n", $ids[$atoms[0]], $ids[$atoms[1]], intval($properties) );
		
		}
		$serial .= "M  END\n";
		
		$attributes = $this->getCollection()->getAttributes( $this->_node->tagName );

		$properties = $xpath->evaluate( 'properties', $this->_node );
		$properties = explode( ',', substr( $properties->item(0)->textContent, 1, -1 ) );
		foreach( $properties as $i => $value ) {
			if( $value[0] == '"' ) {
				$value = substr( $value, 1, -1 );
			}
			$this->_properties[ $attributes[$i] ] = $value;
		}
		
		$this->_serialization = $serial;
	}
	
	public function getName() {
		return $this->_name;
	}

	public function getSerialization() {
		return $this->_serialization;
	}
	
	public function getCollection() {
		return $this->_collection;
	}
	
	public function getCluster() {
		
		if( !$this->_cluster ) {
			foreach( $this->getCollection()->getClusters() as $cluster ) {
				if( $cluster->contains( $this ) ) {
					$this->_cluster = $cluster;
					break;
				}
			}
		}
		return $this->_cluster;
	}
	
	public function getSimilarGraphs() {

		$nGraphs = Clue_Kernel_Conf::get( 'stv.graphs.similar.number', 10 );
		$comparaison = array();
		$id = $this->getId();
		$matrixPointer = fopen( $this->getCollection()->getMatrixPath(), 'r' );
		foreach( $this->getCollection()->getGraphs() as $graph ) {
			if( $graph->getId() == $id ) {
				continue;
			}
			fseek( $matrixPointer, Cluster::getPosition( $id, $graph->getId() ) );
			$sim = fread( $matrixPointer, Cluster::CELL_SIZE );
			$comparaison[] = array( 'sim' => floatval($sim), 'graph' => $graph );
		}
		fclose( $matrixPointer );
		usort( $comparaison, array($this, 'sort' ) );
		return array_slice( $comparaison, 0, $nGraphs );
	}
	
	public function getProperties() {
		return $this->_properties;
	}
	
	public function getProperty( $name ) {
		$this->computes();
		return $this->_properties[ $name ];
	}
	
	public function serialize() {
		$this->_cluster = null;
	}
	
	public function getTypicality() {
		return $this->getCluster()->getTypicality( $this );
	}
	
	public function sort( $a, $b ) {
		if( $a['sim'] == $b['sim'] ) {
			return 0; 
		}
		return $a['sim'] > $b['sim'] ? 1 : -1;
	}
}

?>