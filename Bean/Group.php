<?php

class Group {

	private $_key;
	private $_infos = array();

	public function __construct( $key ) {
		$this->_key = $key;
		if( $this->exists() ) {
			$this->_infos = parse_ini_file( $this->getInfosFile() );
		}
	}

	public static function toKey( $name ) {
		$name = trim($name);
		$name = mb_strtolower( $name );
		return preg_replace( '/[^a-z0-9\-_]/', '',  $name );
	}

	public function isAccessible() {
		if( !isset($_SESSION['user']) ) {
			return false;
		}
		foreach( $_SESSION['user']->getGroups() as $group ) {
			if( $group->getKey() == $this->_key ) {
				return true;
			}
		}
		return false;
	}

	public function create( $name, $owner, $password ) {
		if( !$this->exists() ) {
			mkdir( $this->getDirectory() );
		}
		if( !is_dir($this->getCollectionsDirectory()) ) {
			mkdir( $this->getCollectionsDirectory() );
		}
		$infos = array();
		$infos['name'] = $name;
		$infos['owner'] = $owner;
		if( $password ) {
			$infos['password'] = sha1($password);
		}
		$this->setInfos( $infos );
	}

	public function setInfos( $infos ) {
		$this->_infos = array_merge( $this->_infos, $infos );
		Clue_Kernel_Fs::writeIniFile( $this->getInfosFile(), $this->_infos );
	}

	public function getInfosFile() {
		return $this->getDirectory() . '/group.ini';
	}

	public function getName() {
		return $this->_infos['name'];
	}

	public function getKey() {
		return $this->_key;
	}

	public function exists() {
		return is_dir( $this->getDirectory() );
	}

	public function getDirectory() {
		return self::getGroupsDirectory() . '/' . $this->_key;
	}

	public function getCollectionsDirectory() {
		return $this->getDirectory() . '/collections';
	}

	public function getCollections() {
		$path = $this->getCollectionsDirectory() . '/';
		$dh = opendir( $path );
		$collections = array();
		while( $item = readdir( $dh ) ) {
			if( is_dir( $path . $item ) && $item[0] != '.' ) {
				$collections[] = new Collection( $path . $item );
			}
		}
		return $collections;
	}

	public function getOwner() {
		return $this->_infos['owner'];
	}

	public function hasPassword() {
		return isset($this->_infos['password']);
	}

	public function authenticate($password) {
		return !$this->hasPassword() || (sha1($password) == $this->_infos['password'] );
	}

	public static function getGroupsDirectory() {
		return Clue_Kernel_Conf::get( 'stv.data.directory' ) . '/Groups';
	}

}

