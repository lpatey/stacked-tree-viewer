<?php

class Client {
	
	private $_height;
	
	public function __construct( $height ) {
		$this->setHeight( $height );
	}

	public function getWindowHeight() {
		return $this->_height;
	}
	
	public function setHeight( $height ) {
		$this->_height = $height;
	}
	
	public function getChartHeight() {
		return $this->getWindowHeight() - 80;
	}
	
}

?>
