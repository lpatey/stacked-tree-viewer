<?php

class User {

	private $_isAuthenticated = false;
	private $_currentCollection = null;
	private $_infos = array();

	public function __construct( $login ) {
		$this->_infos['login'] = strip_tags($login);
	}

	public function isAuthenticated() {
		return $this->_isAuthenticated;
	}

	public function authenticate( $password ) {
		if( !$this->exists() ) {
			return false;
		}
		
		$infos = parse_ini_file( $this->getInfosFile() );
		if( sha1($password) != $infos['password'] ) {
			return false;
		}
		$this->_isAuthenticated = true;
		$this->_infos = $infos;

		return true;
	}

	public function setEmail( $email ) {
		$this->_infos['email'] = $email;
		return $this;
	}

	public function setPassword( $password ) {
		$this->_infos['password'] = sha1($password);
		return $this;
	}

	public function joinGroup( $name ) {
		if( is_object($name) ) {
			$name = $name->getKey();
		}
		$groups = array();
		if( @$this->_infos['groups'] ) {
			$groups = explode( ',', $this->_infos['groups'] );
		}
		array_push( $groups, $name );
		$this->_infos['groups'] = implode( ',', $groups );
		return $this;
	}

	public function leaveGroup( $name ) {
		if( is_object($name) ) {
			$name = $name->getKey();
		}
		$groups = array();
		if( @$this->_infos['groups'] ) {
			$groups = explode( ',', $this->_infos['groups'] );
		}
		foreach( $groups as $i => $group ) {
			if( $group == $name ) {
				unset( $groups[$i] );
			}
		}
		$this->_infos['groups'] = implode( ',', $groups );
		return $this;
	}

	public function getGroups() {
		$groups = array();
		foreach( explode( ',', $this->_infos['groups'] ) as $group ) {
			$groups[ $group ] = new Group( $group );
		}
		return $groups;
	}

	public function create( $password, $email ) {
		$this->setPassword($password);
		$this->setEmail($email);
		$this->joinGroup( 'default' );
		$this->save();
	}

	public function save() {
		if( !$this->exists() ) {
			mkdir( $this->getDirectory() );
		}
		if( !is_dir($this->getCollectionsDirectory()) ) {
			mkdir( $this->getCollectionsDirectory() );
		}
		Clue_Kernel_Fs::writeIniFile($this->getInfosFile(), $this->_infos );
	}

	public function getInfosFile() {
		return $this->getDirectory() . '/user.ini';
	}

	public static function getUsersDirectory() {
		return Clue_Kernel_Conf::get( 'stv.data.directory' ) . '/Users';
	}

	public function getDirectory() {
		return self::getUsersDirectory() . '/' . $this->getLogin();
	}

	public function getCollectionsDirectory() {
		return $this->getDirectory() . '/collections';
	}

	public function getLogin() {
		return $this->_infos['login'];
	}

	public function getEmail() {
		return $this->_infos['email'];
	}

	public function exists() {
		return is_dir( $this->getDirectory() );
	}

	public function hasCurrentCollection() {
		return $this->_currentCollection != null;
	}

	public function getCollections() {
		$path = $this->getCollectionsDirectory() . '/';
		$dh = opendir( $path );
		$collections = array();
		while( $item = readdir( $dh ) ) {
			if( is_dir( $path . $item ) && $item[0] != '.' ) {
				$collections[] = new Collection( $path . $item );
			}
		}
		return $collections;
	}

	public function setCurrentCollection( $collection ) {
		$this->_currentCollection = $collection;
		return $this;
	}

	public function getCurrentCollection() {
		return $this->_currentCollection;
	}

}


