<?php

class Collection {
	
	public $POSITION_BY_VALUE = 1;
	public $POSITION_BY_INDEX = 2;
	
	public $HEIGHT_PROPORTIONAL = 3;
	public $HEIGHT_NOT_PROPORTIONAL = 4;
	
	public $CLUSTER_PATTERN = '/([0-9\-]+) +([0-9\-]+) +([0-9.\-]+) +([0-9\-]+)(?: +(#......))?(?: +([^\n]+))?/';

	private $_criteriaViewNumber = null;
	private $_sortCriterion;
	private $_currentCluster = null;
	private $_folder = null;
	private $_available = false;
	private $_name = 'Untitled collection';
	private $_size = 0;
	private $_graphs = null;
	private $_clusters;
	private $_clustersById;
	private $_clustersNumber;
	private $_root = null;
	private $_parentRoot;
	private $_attributes;
	private $_criteria = array();
	private $_computed = false;
	private $_rootSize;
	private $_positionPolicy;
	private $_attributesDomains;
	private $_mergedAttributes;
	private $_rootCluster;
	private $_clustersTreeNormalization;
	private $_clusterMode = 4;
	private $_rootPath = array();
	private $_rootName = 'Root';
	private $_leftSize = 0;
	private $_rightSize = 0;
	private $_allocatedHeight;
	private $_visibleClustersById = array();
	private $_clustersNumberByRoot = array();
	private $_sortAsc = true;
	private $_clustersNumbers = array();
	private $_maxCluster;
	private $_snapshot;
	private $_displayAllGraphs = false;
	
	public function __construct( $folder ) {
		$this->_folder = $folder;
		$this->setClustersNumber(  Clue_Kernel_Conf::get( 'stv.clusters.number', 7 ) );
		$this->_criteriaViewNumber = Clue_Kernel_Conf::get( 'stv.criteria.number' );

		$path = $this->getMetaPath();
		if( file_exists( $path ) ) {
			$metadata = parse_ini_file( $path );
			foreach( $metadata as $key => $value ) {
				$this->{ '_' . $key } = $value;
			}
		}
	}
	
	public function isDisplayAllGraphs() {
		return $this->_displayAllGraphs;
	}
	
	public function setDisplayAllGraphs( $flag ) {
		$this->_displayAllGraphs = $flag;
	}
	
	public function setSortAsc( $sortAsc ) {
		$this->_sortAsc = $sortAsc;
	}
	
	public function isSortAsc() {
		return $this->_sortAsc;
	}
	
	public function setClusterMode( $clusterMode ) {
		$this->_clusterMode = $clusterMode;
		if( $this->_clusters ) {
			$this->_refreshClustersTreeNormalization();
		}
	}
	
	public function getSnapshot() {
		return $this->_snapshot;
	}
	
	public function getClusterMode() {
		return $this->_clusterMode;
	}
	
	public function getRootPath() {
		return $this->_rootPath;
	}
	
	public function getLeftSize() {
		return $this->_leftSize;
	}
	
	public function getRightSize() {
		return $this->_rightSize;
	}
	
	public function getRootName() {
		return $this->_rootName;
	}
	
	public function setCriteriaViewNumber( $criteriaViewNumber ) {
		$this->_criteriaViewNumber = $criteriaViewNumber;
	}
	
	public function getCriteriaViewNumber() {
		return $this->_criteriaViewNumber;
	}
	
	public function getClustersTreeNormalization() {
		return $this->_clustersTreeNormalization;
	}
	
	public function setSortCriterion( $sortCriterion ) {
		$this->_sortCriterion = $sortCriterion;
	}
	
	public function getSortCriterion() {
		return $this->_sortCriterion;
	}
	
	public function setCurrentClusterId( $clusterId ) {
		$this->setCurrentCluster( $this->getCluster( $clusterId ) );
	}
	
	public function setCurrentCluster( $currentCluster ) {
		$this->_currentCluster = $currentCluster;
	}
	
	public function getCurrentCluster() {
		return $this->_currentCluster;
	}
	
	public function getCurrentClusterId() {
		return $this->getCurrentCluster()->getId();
	}
	
	public function isValueAttribute( $attribute = null ) {
		if( !$attribute ) {
			$attribute = $this->getSortCriterion();
			if( !$attribute ) {
				return true;
			}
		}
		return isset($this->_attributesDomains[ $attribute ]);
	}
	
	public function isSortValue() {
		return !$this->_sortCriterion || $this->isValueAttribute( $this->_sortCriterion );
	}
	
	public function getAttributeDomain( $attribute ) {
		return $this->_attributesDomains[ $attribute ];
	}
	
	public function getPositionPolicy() {
		if( isset( $_GET['position_policy'] ) ) {
			 $this->_positionPolicy = $_GET['position_policy'];
		}
		if( !$this->_positionPolicy ) {
			$this->_positionPolicy = $this->POSITION_BY_INDEX;
		}
		if( !$this->isSortValue() ) {
			$this->_positionPolicy = $this->POSITION_BY_INDEX;
		}
		return $this->_positionPolicy;
	}
	
	public function setClustersNumber( $clustersNumber ) {
		$this->_clustersNumber = $clustersNumber;
	}
	
	public function getClustersNumber() {
		return $this->_clustersNumber;
	}
	
	public function getAllocatedHeight() {
		return $this->_allocatedHeight;
	}
	
	public function setGraphVisible( $graph_id ) {
		$hierarchy = file( $this->getClusterPath() );
		$path = $this->getRootPath();
		$current = '-' . ($graph_id + 1);
		$size = count($hierarchy);
		while( ! isset($path['' . ($current)] ) ) {
			for( $i = $current-2; $i<$size; $i++ ) {
				preg_match( $this->CLUSTER_PATTERN, $hierarchy[ $i ], $items );	
				if( $items[1] == $current || $items[2] == $current ) {
					$current = $i + 1;
					if( $current > $size -1 ) {
						$current = 0;
					}
					break;
				}
			}
		}
		unset( $hierarchy );
		$this->setRoot( $current );
		
	}
	
	public function getRoot() {
		return $this->_root;
	}
	
	public function setRoot( $root ) {

		$root = intval( $root );
		
		if( !$this->_root && $root > $this->getSize() - 2 ) {
			return;
		}
		
		if( !$root ) {
			$root = null;
		}
		
		if( $this->_root == $root ) {
			return;	
		}
					
		if( isset( $this->_clustersNumbers[ $root ] ) ) {
			$this->setClustersNumber( $this->_clustersNumbers[ $root ] );
		}
		else {
			if( ($root < $this->_root && $root ) || !$this->_root ) {
				$rootCluster = $this->getCluster( $root );
				if( !$rootCluster ) {
					$visibleClusters = $this->getClustersNumber();
				}
				else {
					$visibleClusters = $rootCluster->getVisibleClusters();
					if( $visibleClusters == 1 ) {
						$visibleClusters = min( $rootCluster->getSize(), $this->getClustersNumber() );
					}
				}
				$this->_root = $root;
				$this->setClustersNumber( $visibleClusters );
				
			}
			else {
				$previousRootCluster = $this->getCluster( $this->_root );
				$splittingCluster = $previousRootCluster->getLowestSplittingCluster();
				$splittingClusterId = intval($splittingCluster->getId());
				
				$hierarchy = file( $this->getClusterPath() );
				$rootValue = $root ? $root : count($hierarchy);
				
				$nClusters = 7;
				$hierarchy = array_slice( $hierarchy, $splittingClusterId-1, $rootValue-$splittingClusterId+1, true );
				$available = array();
				foreach( $hierarchy as $i => $line ) {
					preg_match( $this->CLUSTER_PATTERN, $line, $items );
					$key = '' . ($i+1);
					$available[ $key ] = 0;
					
					
					if( $items[1] < $splittingClusterId ) {
						 $available[ $key ]++; 
					}
					
					if( $items[2] < $splittingClusterId ) {
						 $available[ $key ]++; 
					}
					
					if( isset( $available[ $items[1] ] ) ) {
						$available[ $key ] += $available[ $items[1] ];
						unset( $available[ $items[1] ] );
					}
					
					if( isset( $available[ $items[2] ] ) ) {
						$available[ $key ] += $available[ $items[2] ];
						unset( $available[ $items[2] ] );
					}
				}
				unset( $hierarchy );
	
					
				$clustersNumber = $available[ $rootValue . '' ];
				if( $clustersNumber > Clue_Kernel_Conf::get( 'stv.clusters.max', 64 ) ) {
					$clustersNumber = Clue_Kernel_Conf::get( 'stv.clusters.number', 7 );
				}
				
				$this->setClustersNumber( $clustersNumber );
				
			}
		}
					
		$this->_clusters = array();
		$this->_clustersById = array();
		$this->_visibleClustersById = array();
		$this->_leftSize = 0;
		$this->_rightSize = 0;
		$this->_root = $root;
				
				
		if( !$this->_root ) {
			$this->_rootName = 'Root';
		}
		else {
			$hierarchy = file( $this->getClusterPath() );
			preg_match( $this->CLUSTER_PATTERN, $hierarchy[ $this->_root -1 ], $items );
			$this->_rootName = isset($items[6]) ? $items[6] : 'C' . $this->_root;
			unset( $hierarchy );
		}
	
	}
	
	public function getRootCluster() {
		return $this->_rootCluster;
	}
	
	public function getMergedAttributes() {
		$this->_compute();
		return $this->_mergedAttributes;
	}
	
	public function getCriteria() {
		if( isset( $_POST['change_criteria' ] ) ) {
			$this->_criteria = $_POST['criteria'];
		}
		return $this->_criteria;
	}
	
	public function getCriterionColor( $i ) {
		$criteria = $this->getCriteria();
		return $criteria[$i]['color'];
	}
	
	public function getParentRoot() {
		return $this->_parentRoot;	
	}
	
	public function getRootSize() {
		return $this->_rootSize;
	}
	
	protected function _compute() {
		if( $this->_computed ) {
			return false;
		}
		$this->_computed = true;
		
		$path = $this->getCollectionPath();

		$doc = DOMDocument::load($path,LIBXML_NOBLANKS);
		$id=0;
		$xpath = new DOMXPath( $doc );
	
		$this->_attributes = array();
		foreach( $xpath->evaluate( '/collection/declare/graphs/*' ) as $node ) {
			$graphAttributes = & $this->_attributes[ $node->tagName ];
			foreach( $xpath->evaluate( 'properties/*', $node ) as $property ) {
				$graphAttributes[] = $property->tagName;
				$this->_mergedAttributes[ $property->tagName ] = $property->tagName;
				foreach( $xpath->evaluate( '/collection/declare/macros/' . $property->getAttribute( 'type' ) ) as $type ) {
					if( preg_match( '/^[\{\[]\s*-?([0-9\.]+)\s*,\s*-?([0-9\.]+)\s*[\}\]]$/s', $type->getAttribute( 'domain' ), $match ) ) {
						$this->_attributesDomains[ $property->tagName ] = array( 'min' => floatval( $match[1] ), 'max' => floatval($match[2]) );
					}	
				}
			}
			unset($graphAttributes);
		}
		
		$graphs = array();
		foreach( $xpath->evaluate( '/collection/graphs/*' ) as $node ) {
			$graph = new Graph( $id++, $node->getAttribute( 'id' ), $node, $this );
			$graph->computes();
			$graphs[] = $graph;
		}

		$this->_graphs = $graphs;
		
		
		
	}
	
	public function getFolder() {
		return $this->_folder;
	}
		
	public function getGraph( $id ) {
		$i = '' . $id;
		if( $i[0] == '-' ) {
			$id = abs( intval($id) ) - 1;
		}
		$graphs = $this->getGraphs();
		return $graphs[ $id ];
	}
	
	public function getAttributes( $graph = null ) {
		$this->_compute();
		if( $graph ) {
			return $this->_attributes[$graph];
		}
		return $this->_attributes;
	}
	
	public function getCluster( $id ) {
		if( !$this->_clusters ) {
			$this->clusterize( 6 );
		}
		return $this->_clustersById[$id];
	}
	
	public function getClusters() {
		if( !$this->_clusters ) {
			$this->_clusters = $this->clusterize( 6 );
		}
		return $this->_clusters;
	}
	
	public function getGraphs() {
		$this->_compute();
		return $this->_graphs;
	}
	
	public function clusterize() {

		$root = $this->getRoot();
		
		$n_clusters = $this->getClustersNumber();
		$clusters = array();

		$hierarchy = file( $this->getClusterPath() );
		$n_levels = count($hierarchy);
		
		if( $n_clusters == count((array) $this->_clusters) ) {
			return $this->_clusters;
		}

		if( !$root ) {
			$root = $n_levels;
			$this->_parentRoot = null;
		}
		
		$parents = array_slice( $hierarchy, $root, null, true );
		$leftClusters = array();
		$rightClusters = array();
		$otherClusters = array();
		$visibleClustersById = array();
		
		// Computes root path
		if( !isset( $this->_rootPath[ $this->_root ] ) && $this->_root ) {
			$currentTarget = $root;
			
			$this->_rootPath = array();
			$this->_rootPath[$root] = $this->getRootName();
			foreach( $parents as $i => $line ) {
				preg_match( $this->CLUSTER_PATTERN, $line, $items );
				if( $items[1] == $currentTarget || $items[2] == $currentTarget ) {
					$currentTarget = $i + 1;
					if( $currentTarget < $n_levels ) {
						$this->_rootPath[ $currentTarget ] = isset($items[6]) ? $items[6] : 'C' . $currentTarget;
					}
				}
			}
			if( $parents ) {
				$this->_rootPath[ 0 ] = 'Root';
			}
			$this->_rootPath = array_reverse( $this->_rootPath, true );
		}

		$currentTarget = $root;
		$waitingClusters = array();
		$parents = array_reverse( $parents, true );
		foreach( $parents as $i => $line ) {
			preg_match( $this->CLUSTER_PATTERN, $line, $items );
			if( array_key_exists( $items[1], $this->_rootPath ) || array_key_exists( $items[2], $this->_rootPath ) ) {
				if( array_key_exists( $items[1], $this->_rootPath ) ) {
					$clustersById[ $items[2] ] = $otherClusters[] = $rightClusters[] = new Cluster( $items[2], $i+1, $this, $hierarchy );
				}
				else {
					$clustersById[ $items[1] ] =  $otherClusters[] = $leftClusters[] = new Cluster( $items[1], $i+1, $this, $hierarchy );
				}
				$currentTarget = $i + 1;
			}
			else {
	
				foreach( $otherClusters as $cluster ) {
					if( $cluster->contains( '' . ($i+1) ) ) {
						$cluster->add( $items[1] );
						$cluster->add( $items[2] );
						break;
					}
				}
			}
		}
		
		$keys = array_keys( $this->_rootPath );
		$index = array_search( $root, $keys );
		$this->_parentRoot = intval( $index ? $keys[$index-1] : end($keys) );
		
		$hierarchy = array_slice( $hierarchy, 0, $root, true );
		$n_levels = $root;
		$n_found = 1;

		$this->_rootCluster = $clustersById[ $root ] = new Cluster( $root, null, $this, $hierarchy );
		
		for( $i=0; $n_found < $n_clusters && $i<$n_levels; $i++ ) {
			$line = array_pop( $hierarchy );
			preg_match( $this->CLUSTER_PATTERN, $line, $items );
			if( isset($visibleClustersById[ $root - $i ]) || !$visibleClustersById ) {
				$parentCluster = $clustersById[ $root-$i ];
				$visibleClustersById[ $items[1] ] = $clustersById[ $items[1] ] = $leftCluster = new Cluster( $items[1], $root-$i, $this, $hierarchy );
				$visibleClustersById[ $items[2] ] = $clustersById[ $items[2] ] = $rightCluster = new Cluster( $items[2], $root-$i, $this, $hierarchy );
				$parentCluster->addChildCluster( $leftCluster );
				$parentCluster->addChildCluster( $rightCluster );
				//$parentCluster->setDistance( floatval($items[3]) );
				unset( $visibleClustersById[ $root-$i ] );
				$n_found++;
			}
			else {
				foreach( $otherClusters as $cluster ) {
					if( $cluster->contains( $root-$i ) ) {
						$cluster->add( $items[1] );
						$cluster->add( $items[2] );
						break;
					}
				}
			}
		}
		
		$this->_clustersById = $clustersById;
		$clusters = $this->_rootCluster->getClustersList();

		$i = count($hierarchy);
		while( $line = array_pop( $hierarchy ) ) {
			preg_match( $this->CLUSTER_PATTERN, $line, $items );
			$found = false;
			foreach( $clusters as $cluster ) {			
				if( $cluster->contains( $i ) ) {
					$cluster->add( $items[1] );
					$cluster->add( $items[2] );
					$found = true;
					break;
				}
			}
			if( !$found ) {
				foreach( $otherClusters as $cluster ) {
					if( $cluster->contains( $i ) ) {
						$cluster->add( $items[1] );
						$cluster->add( $items[2] );
						break;
					}
				}
			}
			$i--;
			
		}
	
		$this->setClustersNumber( count($clusters) );
		$this->_clustersNumbers[ $this->_root ] = count($clusters);
		
		if( !$this->_leftSize ) {
			$this->_leftSize = 0;
			foreach( $leftClusters as $cluster ) {
				$this->_leftSize += $cluster->getSize();
			}
		}
		if( !$this->_rightSize ) {
			$this->_rightSize = 0;
			foreach( $rightClusters as $cluster ) {
				$this->_rightSize += $cluster->getSize();
			}		
		}
		
		$this->_allocatedHeight = floatval(Clue_Kernel_Conf::get( 'stv.chart.bar.height', 60 ));
		$max = 0;
		$sum = 0;
		$i = 0;
		$barLeft = 100/$this->getClustersNumber();
		foreach( $clusters as $cluster ) {
			$max = max($cluster->getSize(), $max );
			if( $cluster->getSize() == $max ) {
				$this->_maxCluster = $cluster;
			}
			$sum += $cluster->getSize();
			$cluster->setLeft( $barLeft*($i) + ($barLeft-$cluster->getWidth())*0.5 );
			$i++;
		}
		$this->_rootSize = $sum;
		$client = $_SESSION['client'];
		$maxHeight = floatval(Clue_Kernel_Conf::get( 'stv.chart.bar.unit', 3 ))*$max*100/$client->getChartHeight();
		$this->_allocatedHeight = min( $this->_allocatedHeight, $maxHeight );
		
	
		foreach( $clusters as $cluster ) {
			$cluster->setPercentage( $cluster->getSize()*100 / $max );
		
		}
		
		$this->_refreshClustersTreeNormalization();
		
		if( $this->_currentCluster ) {
			foreach( $clusters as $cluster ) {
				if( $this->_currentCluster->getId()  == $cluster->getId() ) {
					$this->_currentCluster = $cluster;
					$found = true;
					break;
				}
			}
		}
			
		return $this->_clusters = $clusters;
	}
	
	private function _refreshClustersTreeNormalization() {
		$this->_clustersTreeNormalization = $this->_rootCluster->getHeadTop()/(85-$this->getAllocatedHeight());
		$this->getRootCluster()->resetHeadTop();
	}
	
	public function getPath( $file ) {
		return $this->getFolder() . '/' . $file;
	}
	
	public function getMetaPath() {
		return $this->getPath( 'collection.ini' );
	}
	
	public function getMatrixPath() {
		return $this->getPath( 'collection.matrix' );
	}
	
	public function getClusterPath() {
		return $this->getPath( 'collection.clusters' );
	}
	
	public function getCommentsPath() {
		return $this->getPath( 'comments.txt' );
	}
	
	public function getCollectionPath() {
		return $this->getPath( 'collection.irln' );
	}
	
	public function getSnapshotPath() {
		return $this->getPath( 'snapshots/' . $this->_snapshot . '.srl' );
	}
	
	public function getSnapshots() {
		$path = $this->getPath( 'snapshots' );
		foreach( glob( $path . '/*.srl' ) as $snapshot ) {
			$snapshots[] = basename( $snapshot, '.srl' );
		}
		return $snapshots;
	}
	
	public function getIniInfo( $key ) {
		$infos = parse_ini_file( $this->getMetaPath() );
		return $infos[$key];
	}
	
	public function setIniInfo( $key, $value ) {
		$infos = parse_ini_file( $this->getMetaPath() );
		$infos[ $key ] = $value;
		Clue_Kernel_Fs::writeIniFile( $this->getMetaPath(), $infos );
	}
	
	public function save( $snapshot = null ) {
		
		if( $snapshot ) {
			$this->_snapshot =  $snapshot;
			$this->setIniInfo( 'snapshot', $snapshot );
		}
		
		if( !$this->_snapshot ) {
			return;
		}
		
		$path = $this->getSnapshotPath();
		
		
		file_put_contents( $path, serialize( $this ) );
	}
	
	public function restore( $snapshot = null ) {
		
		if( $snapshot ) {
			$this->save();
			$this->_snapshot = $snapshot;
			$this->setIniInfo( 'snapshot', $snapshot );
		}
		
		
		$path = $this->getSnapshotPath();

		
		if( file_exists( $path ) ) {
			return unserialize( file_get_contents( $path ) );
		}
		else {
			$this->save();
		}
		
		return $this;
	}
	
	public function unselectCluster() {
		$this->_currentCluster = null;
	}
	
	public function getSize() {
		return $this->_size;
	}
	
	public function getName() {
		return $this->_name;
	}
	
	public function getPercentageOfWholeCollection() {
		return $this->_rootSize*100/$this->_size;
	}
	
	public function getMaxCluster() {
		return $this->_maxCluster;
	}
	
	public function isAvailable() {
		return $this->_available;
	}
	
	public function reset() {
		$this->_root = null;
		$this->_clustersNumbers = array();
		$this->_rootPath = array();
		$this->setClustersNumber(  Clue_Kernel_Conf::get( 'stv.clusters.number', 7 ) );
	}
}

?>
