<?php

class Cluster {
	
	// CELL_SIZE = number size + 1
	const CELL_SIZE = 10;

	private $_id;
	private $_name;
	private $_percentage;
	private $_size = 0;
	private $_content = array();
	private $_flags = array();
	private $_graphs;
	private $_graphsByTypicality;
	private $_graphsCriteria;
	private $_criteriaHash;
	private $_parent;
	private $_collection;
	private $_typicalId;
	private $_sort = -1;
	private $_simMatrixPointer;
	private $_typicalities = array();
	private $_typicalitiesIndexes = array();
	private $_sortIndexes = array();
	private $_leftChildCluster = null;
	private $_rightChildCluster = null;
	private $_proportionalHeadTop = null;
	private $_notProportionalHeadTop = null;
	private $_headMiddle = null;
	private $_left;
	private $_distance;
	private $_visibleClusters;
	private $_color;

	public function __construct( $id, $parent, $collection, $hierarchy ) {
		$this->_id = $id;
		$this->setTypicalId($id);
		$this->add( $id );
		
		if( $id && isset($hierarchy[$id-1])) {
			preg_match( $collection->CLUSTER_PATTERN, $hierarchy[$id-1], $items );
			$this->_name = isset($items[6]) ? $items[6] :  'C' . $id;
			$this->_color = isset($items[5]) ? $items[5] : null;
			$this->_distance = floatval($items[3]);
			$this->_typicalId = $id[0] == '-' ? substr( $id, 1 ) - 1 : $items[4] -1;
		}
		else {
			$this->_name = 'Root';
		}
		$this->_collection = $collection;
		$this->_parent = $parent;
	}
	
	public function import( $cluster ) {
		foreach( $cluster->_content as $item => $true ) {
			$this->add( $item );
		}
		foreach( $cluster->_flags as $item => $true ) {
			$this->add( $item );
		}
		$this->add( $cluster->getId() );
	}
	
	public function setDistance( $distance ) {
		$this->_distance = $distance;
	}
	
	public function getDistance() {
		return $this->_distance;
	}
	
	public function setColor( $color ) {
		$this->_color = $color;
	}
	
	public function getColor() {
		return $this->_color;
	}
	
	public function setLeft( $left ) {
		$this->_left = $left;
	}
	
	public function hasChildrenClusters() {
		return $this->_leftChildCluster != null;
	}
	
	public function addChildCluster( $cluster ) {
		if( !$this->_leftChildCluster ) {
			$this->_leftChildCluster = $cluster;
		}
		else {
			$this->_rightChildCluster = $cluster;
		}
	}
	
	public function getLeft() {
		return $this->_left;
	}
	
	public function getHeight() {
		return $this->getPercentage()*$this->getCollection()->getAllocatedHeight()/100;
	}

	
	public function getWidth() {
		return 60/$this->getCollection()->getClustersNumber();
	}
	
	public function resetHeadTop() {
		$this->_notProportionalHeadTop = null;
		$this->_proportionalHeadTop = null;
		if( $this->hasChildrenClusters() ) {
			$this->_leftChildCluster->resetHeadTop();
			$this->_rightChildCluster->resetHeadTop();
		}
		
	}
	
	public function getLowestSplittingCluster() {

		if( !$this->_leftChildCluster ) {
			if( $this->_rightChildCluster->hasChildrenClusters() ) {
				return $this->_rightChildCluster->getLowestSplittingCluster();
			}
			else {
				return $this;
			}
		}
		else if( !$this->_rightChildCluster ) {
			if( $this->_leftChildCluster->hasChildrenClusters() ) {
				return $this->_leftChildCluster->getLowestSplittingCluster();
			}
			else {
				return $this;
			}
		}
		else {
			
			if( $this->_leftChildCluster->hasChildrenClusters() ) {
				$left = $this->_leftChildCluster->getLowestSplittingCluster();	
			}
			
			if( $this->_rightChildCluster->hasChildrenClusters() ) {
				$right = $this->_rightChildCluster->getLowestSplittingCluster();	
			}
			
			if( !$left && !$right ) {
				return $this;
			}
			
			if( !$left ) {
				return $right;
			}
			
			if( !$right ) {
				return $left;
			}
			
			return $left->getId() < $right->getId() ? $left : $right;
		}
	}
	
	public function getVisibleClusters() {
		if( $this->hasChildrenClusters() ) {
			return $this->_leftChildCluster->getVisibleClusters() 
				+ $this->_rightChildCluster->getVisibleClusters();
		}
		return 1;
	}
	
	public function getProportionalHeadTop() {
		if( !$this->_proportionalHeadTop ) {
				$min = $this->getCollection()->getMaxCluster()->getDistance();
				$this->_proportionalHeadTop = $this->getDistance() - $min;
			
		}
		return $this->_proportionalHeadTop;
	}
	
	public function getNotProportionalHeadTop() {
		if( !$this->_notProportionalHeadTop ) {
			if( $this->hasChildrenClusters() ) {
				$left = $this->_leftChildCluster->getNotProportionalHeadTop();
				$right = $this->_rightChildCluster->getNotProportionalHeadTop();
				$this->_notProportionalHeadTop = max($left,$right)+1;
			}
			else {
				$this->_notProportionalHeadTop = 0;
			}
		}
		return $this->_notProportionalHeadTop;
	}
	
	public function getHeadTop() {
		switch( $this->getCollection()->getClusterMode() ) {
			case $this->getCollection()->HEIGHT_NOT_PROPORTIONAL :
				return $this->getNotProportionalHeadTop();
			case $this->getCollection()->HEIGHT_PROPORTIONAL :
				return $this->getProportionalHeadTop();
			default :
				return $this->getProportionalHeadTop();
		}
	}
	
	public function getHeadMiddle() {
		if( !$this->_headMiddle ) {
			if( $this->hasChildrenClusters() ) {
				$left = $this->_leftChildCluster->getHeadMiddle();
				$right = $this->_rightChildCluster->getHeadMiddle();
				$this->_headMiddle = ($left+$right)/2;
			}
			else {
				$this->_headMiddle = $this->getLeft()+$this->getWidth()/2;
			}
		}
		return $this->_headMiddle;
	}
	
	public function getCollection() {
		return $this->_collection;
	}
	
	public function getParent() {
		return $this->_parent;
	}
	
	public function getTypicalities() {
		return $this->_typicalities;
	}
	
	public function getTypicalityIndex( $graph_id ) {;
		return $this->_typicalitiesIndexes[ $graph_id ];
	}
	
	public function getSortIndex( $graph_id ) {
		return $this->_sortIndexes[ $graph_id ];
	}
	
	public function getNormalizedSortValue( $graph_id )  {
		$graph = $this->getCollection()->getGraph( $graph_id );
		if( !$this->sort()->_sort ) {
			return $this->getTypicality($graph);
		}
		$domain = $this->getCollection()->getAttributeDomain( $this->_sort );
		return ($graph->getProperty( $this->_sort )-$domain['min']) / ($domain['max'] - $domain['min']);
	}
	
	public function sort() {
		$sort = $this->getCollection()->getSortCriterion();
		if( $sort == $this->_sort ) {
			return $this;
		}
		
		$this->_sort = $sort;
		$this->_graphsCriteria = array();
		$this->_criteriaHash = null;
	
		$graphs = $this->getGraphs();
		
		if( $sort ) {
			usort( $graphs, array($this, '_sortAlgorithm' ) );
			foreach( $graphs as $i => $graph ) {
				$this->_sortIndexes[ $graph->getId() ] = $i;
			}
		}
		else {
			$graphs = $this->getGraphsByTypicality();
			$this->_sortIndexes = $this->_typicalitiesIndexes;
		}
		
		$this->_graphs = $graphs;
		
		return $this;
	}
	
	public function getSort() {
		return $this->_sort;
	}
	
	public function getGraphsCriteria() {
		
		$this->sort();
		
		$criteria = $this->getCollection()->getCriteria();
		$hash = md5(serialize($criteria));
		if( $hash != $this->_criteriaHash )  {
			$this->_criteriaHash = $hash;
			$this->_graphsCriteria = array();
			if( !$criteria ) {
				return array();	
			}
			
			$simPointer = fopen( $this->getCollection()->getMatrixPath(), 'r' );
			foreach( $this->getGraphs() as $graph ) {
				foreach( $criteria as $i => $criterion ) {
					$value = $criterion['criterion'] ? 
						$graph->getProperty( $criterion['criterion'] ) : 
						$this->getTypicality( $graph, $simPointer );
					
					$verifies = false;
					switch( $criterion['operator'] ) {
						case '<' :
							$verifies = $value < $criterion['value'];
							break;
						case '>' :
							$verifies = $value > $criterion['value'];
							break;
						case '==' :
							$verifies = $value == $criterion['value'];
							break;
						case '!=' :
							$verifies = $value != $criterion['value'];
							break;
						default :
							trigger_error( 'Unknown operator', E_USER_ERROR );
					}
					if( $verifies ) {
						$this->getTypicality( $graph, $simPointer );
						$this->_graphsCriteria[$graph->getId()][] = $criterion;
					}
				}
			}
			fclose( $simPointer );

		} 
		
		if( $this->getCollection()->isSortAsc() ) {
			return (array) $this->_graphsCriteria;
		}
		
		return array_reverse( (array) $this->_graphsCriteria, true );
	}
	
	public function getTypicalGraph() {
		return $this->getCollection()->getGraph( $this->getTypicalId() );
	}
	
	public function getGraphsByTypicality() {
		
		if( !$this->_graphsByTypicality ) {
			$this->_simMatrixPointer = fopen( $this->getCollection()->getMatrixPath(), 'r' );
			$graphs = $this->getGraphs();
			usort( $graphs, array($this, '_sortByTypicallityAlgorithm' ) );
			@fclose( $this->_simMatrixPointer );
			$i=0;
			foreach( $graphs as $graph ) {
				$this->_typicalitiesIndexes[ $graph->getId() ] = $i++;
			}
			$this->_graphsByTypicality = $graphs;
		}
		
		return $this->_graphsByTypicality;
	}
	
	public function setTypicalId( $id ) {
		$i = '' . $id;
		if( $i[0] == '-' ) {
			$id = abs( intval($id) ) - 1;
		}
		$this->_typicalId = $id;
	}
	
	public function getTypicalId() {
		return $this->_typicalId;
	}
	
	public function getId() {
		return $this->_id;
	}
	
	public function setName( $name ) {
		$this->_name = $name;
	}
	
	public function getGraphs() {
		if( $this->_graphs == null ) {
			$graphs = array();
			foreach( $this->_content as $id => $true ) {
				$graphs[ $id ] = $this->getCollection()->getGraph( $id );
			}
			$this->_graphs = $graphs;
		}
		$this->sort();

		return $this->_graphs;
	}
	
	public function getOrderedGraphs() {
		if( $this->getCollection()->isSortAsc() ) {
			return array_reverse( $this->getGraphs(), true );
		}
		return $this->getGraphs();
	}
	
	public function contains( $item ) {
		return is_object( $item ) ? in_array( $item, $this->getGraphs() ) : isset( $this->_flags[ '' . $item ] );
	}
	
	public function add( $item ) {
		if( $item[0] == '-' ) {
			$this->_content[ $item ] = true;
			$this->_size++;
		}
		else {
			$this->_flags[ $item ] = true;
		}
	}
	
	public function setPercentage( $percentage ) {
		$this->_percentage = $percentage;
	}
	
	public function setSize( $size ) {
		$this->_size = $size;
	}
	
	public function getSize() {
		if( !$this->_size && $this->hasChildrenClusters() ) {
			$this->_size = $this->_leftChildCluster->getSize() + $this->_rightChildCluster->getSize();
		}
		return $this->_size;
	}
	
	public function getPercentage() {
		return $this->_percentage;
	}
	
	public function getName() {
		return $this->_name;
	}
	
	private function _sortAlgorithm( $a, $b ) {
		return strnatcmp( $a->getProperty( $this->_sort ), $b->getProperty( $this->_sort ) );	
	}
	
	public function getTypicality( $a, $matrixPointer = null ) {
		$id = $a->getId();
		if( !isset($this->_typicalities[ $id ]) ) {
			if( $id == $this->getTypicalId() ) {
				return $this->_typicalities[ $id ] = 1;
			}
			if( !$matrixPointer ) {
				throw new Exception( 'Error with graph ' . $id );
			}
			fseek( $matrixPointer, $this->getPosition( $id, $this->getTypicalId() ) );
			$simA = fread( $matrixPointer, self::CELL_SIZE );
			
			$this->_typicalities[ $id ] = $simA;
		
		}
		return $this->_typicalities[ $id ];
	}
	
	
	public static function getPosition( $i, $j ) {
		$min = min($i,$j);
		$max = max($i,$j)-1;
		// Le +max final correspond aux retours à la ligne
		return $max*($max+1)*self::CELL_SIZE/2 + $min*self::CELL_SIZE + $max;
	}
	
	private function _sortByTypicallityAlgorithm( $a, $b ) {
		
		$simA = $this->getTypicality($a, $this->_simMatrixPointer);
		$simB = $this->getTypicality($b, $this->_simMatrixPointer);
		return strnatcmp( $simA, $simB );
	}
	
	public function getClustersList() {
		if( !$this->hasChildrenClusters() ) {
			return array( $this );
		}
		
		return array_merge( $this->_leftChildCluster->getClustersList(), $this->_rightChildCluster->getClustersList() );
	}
	
	public function getLowestParentCluster() {
		$id = $this->getId();
		if( $this->_leftChildCluster->hasChildrenClusters() ) {
			$id = min( $id, $this->_leftChildCluster->getLowestParentCluster() );
		}
		if( $this->_rightChildCluster->hasChildrenClusters() ) {
			$id = min( $id, $this->_rightChildCluster->getLowestParentCluster() );
		}
		return $id;
	}
	
	public function getHierarchyHeight() {
		return $this->getCollection()->getAllocatedHeight() + $this->getHeadTop()/$this->getCollection()->getClustersTreeNormalization();
	}
	
	public function displayAsAncestor() {
		if( !$this->hasChildrenClusters() ) {
			return;
		}
		
		$left = $this->_leftChildCluster->getHeadMiddle();
		$right = $this->_rightChildCluster->getHeadMiddle();
		$width = $right - $left;
		$rootPath = $this->getCollection()->getRootPath();
		$bottom = 0;
		//$leftHeight = 
		$height = $this->getHierarchyHeight();
		$leftSelected  = array_key_exists($this->_leftChildCluster->getId(), $rootPath );
		$rightSelected = array_key_exists($this->_rightChildCluster->getId(), $rootPath );
		// parent with children
		$parentSelected = 	$leftSelected  || $rightSelected;
		// node in the current path
		$nodeSelected  = ($parentSelected || array_key_exists($this->getId(), $rootPath ));
		$id =  $this->getId() > $this->getCollection()->getSize() - 2  ? 0 : $this->getId();

		
		echo '<div>';
		echo '<div head_top="' . $this->getProportionalHeadTop() . '" cluster_id="' . $id . '" class="' .( $nodeSelected ? 'Selected ' : '' ) . 'ClusterContainer" style="left: ' . $left . '%; width: ' . $width . '%; height: ' . $height . '%">';
		echo '<div
			 left_cluster_id="' . $this->_leftChildCluster->getId() . '"
			 class="Left ParentCluster ' . ( $nodeSelected && !$leftSelected && $parentSelected ? ' Dimmed' : '' ) . '"></div>';
		echo '<div
			 right_cluster_id="' . $this->_rightChildCluster->getId() . '"
			 class="Right ParentCluster ' . ( $nodeSelected && !$rightSelected && $parentSelected ? ' Dimmed' : '' ) . '"></div>';
		// visualize all the nodes within the path
		echo '<div class="SetAsRoot ' . ($nodeSelected  ? ' Selected' : '' ) . '" title="#' . $this->getName() . ' ' . $this->getSize() . ' items"></div>';
		echo '</div>';
		
		
		$this->_leftChildCluster->displayAsAncestor();
		$this->_rightChildCluster->displayAsAncestor();
		echo '</div>';
		
	}
}

?>
